Steps to install:

1. Install Gulp:
 Open a command prompt and then do "npm install -g gulp".
 This will install gulp in global npm folder.

2. Install Cordova:
 Open a command prompt and then do "npm install -g cordova".
 This will install cordova in global npm folder.

3. Install npm:
 Open a command prompt and then cd to the folder containing project, and do "npm install".
 This will install npm packages, after this you will be able to see "node_modules" folder in the project directory.

4. Install bower:
 Open a command prompt and then cd to the folder containing project, and do "bower install".
 This will install bower components, after this you will be able to see "lib" folder in "app" folder of the project directory.

5. Install Plugins & Platforms:
 Open a command prompt and then do "gulp tasks".
 This will install all the required plugins and platforms, and you will be able to see "plugins" and "platforms" folders in the project directory.

6. Run application:
 To do this, open a command prompt and then cd to the folder containing project, and do "gulp".