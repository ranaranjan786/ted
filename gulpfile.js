var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var templateCache = require('gulp-angular-templatecache');
var ngAnnotate = require('gulp-ng-annotate');
var useref = require('gulp-useref');
var clean = require('gulp-rimraf');
var shell = require('gulp-shell')

var paths = {
  sass: ['./scss/**/*.scss'],
  templatecache: ['./www/templates/**/*.html'],
  ng_annotate: ['./www/js/*.js'],
  useref: ['./www/*.html']
};

gulp.task('ng_annotate',['sass'], function (done) {
  gulp.src('./www/js/**/*.js')
    .pipe(ngAnnotate({ single_quotes: true }))
    .pipe(gulp.dest('./www/dist/dist_js/app'))
    .on('end', done);
});


gulp.task('templatecache',['useref'], function (done) {
  gulp.src('./www/templates/**/*.html')
    .pipe(templateCache({ standalone: true }))
    .pipe(gulp.dest('./www/js'))
    .on('end', done);
});

// gulp.task('default', ['watch'],function(done){

// });

gulp.task('default', ['watch'], shell.task([
  'ionic serve'
]));

gulp.task('watch', ['templatecache'], function () {
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.templatecache, ['templatecache']);
  gulp.watch(paths.ng_annotate, ['ng_annotate']);
  gulp.watch(paths.useref, ['useref']);
});

gulp.task('clean', function() {
  console.log("Clean all files in build folder");

  return gulp.src("./www/dist/*", { read: false }).pipe(clean());
});


gulp.task('sass',['clean'], function (done) {
  gulp.src('./scss/ionic.app.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});



gulp.task('install', ['git-check'], function () {
  return bower.commands.install()
    .on('log', function (data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('useref',['ng_annotate'] ,function (done) {
  var assets = useref();
  gulp.src('./www/*.html')
    // .pipe(assets)
    // .pipe(assets.restore())
    .pipe(useref())
    .pipe(gulp.dest('./www/dist'))
    .on('end', done);
});

gulp.task('git-check', function (done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});

