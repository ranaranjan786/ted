// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'ionic-material', 'ionMdInput','route','services','angularXml2json' ,'ngSanitize','templates'])

.run(['$ionicPlatform', function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
}]);

angular.module('templates', []).run(['$templateCache', function($templateCache) {$templateCache.put('feedDetails.html','<ion-view view-title="Feed Detail">\n    <ion-content ng-class="{expanded:isExpanded}" class="animate-fade-slide-in">\n        <div class="list half" style="width: 100% !important;">\n            <i class="fa fa-arrow-left" aria-hidden="true" ui-sref="app.feeds"></i>\n            <div class="card card-gallery item item-text-wrap">\n                <div class="ink dark-bg">\n                    <!--<img class="full-image" ng-src="{{itemObj.mediathumbnail.url|replaceDash}}"> {{itemObj.mediacontent.url|replaceDash}}-->\n                    <video class="full-image" controls>\n                        <source ng-src="{{trustSrc(itemObj.mediacontent.url)}}" type="video/mp4">\n                    </video>\n                </div>\n            </div>\n            <span>{{itemObj.itunesauthor}}</span>\n            <h2 style="font-weight: bold;">{{itemObj.itunessubtitle}}</h2>\n        </div>\n\n    </ion-content>\n</ion-view>');
$templateCache.put('feeds.html','<style>\n    .txt-color {\n        color: #ffffff;\n    }\n</style>\n\n<ion-view view-title="Feed" ng-if="feeds.length >0">\n    <ion-content class="animate-fade-slide-in">\n        <div class="item card-item" ng-repeat="feed in feeds" style="height: 33%">\n            <div ui-sref="app.feeddetail({\'id\':feed.jwplayertalkid})" class="card stable-bg ink ink-dark" style=" background: #ffffff url({{feed.mediathumbnail.url|replaceDash}}) no-repeat right top;height: 100%">\n                <div class="item item-avatar item-text-wrap" style="padding-left: 5%;">\n                    <span class="avatar"></span>\n                    <strong class="txt-color" style="font-size: 26px;">{{feed.itunesauthor}}</strong>\n                    <div class="row txt-color" style="font-size: 16px;">\n                        {{feed.itunessubtitle}}\n                    </div>\n                    <div class="card-footer text-right txt-color" style="padding-top: 27% !important;">\n                        {{feed.itunesduration|timeFilter}}\n                    </div>\n                </div>\n            </div>\n        </div>\n    </ion-content>\n</ion-view>');
$templateCache.put('menu.html','<ion-side-menus enable-menu-with-back-views="true">\n    <ion-side-menu-content>\n        <ion-nav-bar class="bar-assertive-900" ng-class="{expanded: isExpanded, \'has-header-fab-left\': hasHeaderFabLeft, \'has-header-fab-right\': hasHeaderFabRight}" align-title="left">\n         \n        </ion-nav-bar>        \n        <ion-nav-view name="menuContent" ng-class="{expanded: isExpanded}" ></ion-nav-view>\n    </ion-side-menu-content>\n  \n</ion-side-menus>\n');}]);
(function () {
    angular.module('route', [])  
.config(['$stateProvider', '$urlRouterProvider', '$ionicConfigProvider', function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    $ionicConfigProvider.views.maxCache(0);
    $stateProvider.state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'menu.html',
        controller: 'AppCtrl'
    })
    .state('app.feeds', {
        url: '/feeds',
        views: {
            'menuContent': {
                templateUrl: 'feeds.html',
                controller: 'FeedCtrl'
            }        
        }
    })

  .state('app.feeddetail', {
        url: '/FeedDetail/:id',
        views: {
            'menuContent': {
                templateUrl: 'feedDetails.html',
                controller: 'FeedDetailCtrl'
            }
        }
    });
  
    $urlRouterProvider.otherwise('/app/feeds');
}]);

})();
/* global angular, document, window */


angular.module('starter.controllers', [])

    .controller('AppCtrl', ['$scope', '$ionicModal', '$ionicPopover', '$timeout', function ($scope, $ionicModal, $ionicPopover, $timeout) {


        var navIcons = document.getElementsByClassName('ion-navicon');
        for (var i = 0; i < navIcons.length; i++) {
            navIcons.addEventListener('click', function () {
                this.classList.toggle('active');
            });
        }

        ////////////////////////////////////////
        // Layout Methods
        ////////////////////////////////////////

        $scope.hideNavBar = function () {
            document.getElementsByTagName('ion-nav-bar')[0].style.display = 'none';
        };

        $scope.showNavBar = function () {
            document.getElementsByTagName('ion-nav-bar')[0].style.display = 'block';
        };

        $scope.noHeader = function () {
            var content = document.getElementsByTagName('ion-content');
            for (var i = 0; i < content.length; i++) {
                if (content[i].classList.contains('has-header')) {
                    content[i].classList.toggle('has-header');
                }
            }
        };

        $scope.setExpanded = function (bool) {
            $scope.isExpanded = bool;
        };

        $scope.setHeaderFab = function (location) {
            var hasHeaderFabLeft = false;
            var hasHeaderFabRight = false;

            switch (location) {
                case 'left':
                    hasHeaderFabLeft = true;
                    break;
                case 'right':
                    hasHeaderFabRight = true;
                    break;
            }

            $scope.hasHeaderFabLeft = hasHeaderFabLeft;
            $scope.hasHeaderFabRight = hasHeaderFabRight;
        };

        $scope.hasHeader = function () {
            var content = document.getElementsByTagName('ion-content');
            for (var i = 0; i < content.length; i++) {
                if (!content[i].classList.contains('has-header')) {
                    content[i].classList.toggle('has-header');
                }
            }

        };

        $scope.hideHeader = function () {
            $scope.hideNavBar();
            $scope.noHeader();
        };

        $scope.showHeader = function () {
            $scope.showNavBar();
            $scope.hasHeader();
        };

        $scope.clearFabs = function () {
            var fabs = document.getElementsByClassName('button-fab');
            if (fabs.length && fabs.length > 1) {
                fabs[0].remove();
            }
        };
    }])
    .controller('FeedCtrl', ['$scope', '$stateParams', '$timeout', 'ionicMaterialMotion', 'ionicMaterialInk', 'service', 'xmlConstant', 'ngXml2json', '$ionicLoading', '$sce', function ($scope, $stateParams, $timeout, ionicMaterialMotion, ionicMaterialInk, service, xmlConstant, ngXml2json, $ionicLoading, $sce) {

        // We are not calling "https://pa.tedcdn.com/feeds/talks.rss" API because it throws error "No 'Access-Control-Allow-Origin' header is present on the requested resource".
 $ionicLoading.show({ template: 'Please wait' });
        service.getFeeds().then(function (res) {
            // console.log('res', res);
            if (!angular.fromJson(localStorage.getItem("items"))) {
                var jsonObject = ngXml2json.parser(res.data); // converting xml data into json.
                $scope.feeds = jsonObject.rss.channel.item;
                localStorage.setItem("items", angular.toJson($scope.feeds)); // storing feed in localstorage for better performance.
                $ionicLoading.hide();
            }
            else {
               $scope.feeds = angular.fromJson(localStorage.getItem("items")); // get feeds from localstorage if already downladed feeds available.
                $ionicLoading.hide();
              //  $scope.$apply();
            }
        });

       


        $timeout(function () {    // loading hide for 2 second
            $ionicLoading.hide();
        }, 2000);

        $timeout(function () {
            ionicMaterialMotion.fadeSlideIn({
                selector: '.animate-fade-slide-in .item'
            });
        }, 1000);

        // Activate ink for controller
        // ionicMaterialInk.displayEffect();
    }])
    .filter('replaceDash', function () {        // custome filter for replacing  '-' with :.
        return function (url) {
            return url.replace('https-', 'https:');
        };
    })

    .filter('timeFilter', function () {   // custome filter for remove  '00-'.
        return function (time) {
            return time.replace('00-', '');
        };
    })
    .controller('FeedDetailCtrl', ['$scope', '$stateParams', '$timeout', 'ionicMaterialInk', 'ionicMaterialMotion', '$filter', '$sce', function ($scope, $stateParams, $timeout, ionicMaterialInk, ionicMaterialMotion, $filter, $sce) {
        var id = $stateParams.id;   // getting feedID from queryString. 
        var items = angular.fromJson(localStorage.getItem("items"));    // getting feeds from localStorage.
        if (items) {
            $scope.itemObj = $filter('filter')(items, function (item) {
                return id == item.jwplayertalkid;                                   // filter feed details for selected feed.
            })[0];
        }
        $scope.trustSrc = function (url) {
            url = url.replace('https-', 'https:');
            return $sce.trustAsResourceUrl(url);
        };

        $scope.$parent.showHeader();
        $scope.$parent.clearFabs();
        $scope.isExpanded = true;
        $scope.$parent.setExpanded(true);
        $scope.$parent.setHeaderFab(false);

        // Activate ink for controller
        ionicMaterialInk.displayEffect();

        ionicMaterialMotion.pushDown({
            selector: '.push-down'
        });
        ionicMaterialMotion.fadeSlideInRight({
            selector: '.animate-fade-slide-in .item'
        });

    }]);

(function () {
    angular.module('services', [])
        .service('service', ['$http',  function ($http) {

            this.getFeeds = function () {
                return $http({
                    method: "GET",
                    dataType:'JSONP',
                    // url: 'http://pa.tedcdn.com/feeds/talks.rss',
                    url: 'xml.js',
                    crossDomain: true
                })
                    .success(function (response) {
                        console.log(response);
                    })
                    .error(function (error) {
                        console.log(error);
                    });
            };
        }])
        .constant('xmlConstant',{
            xml:''
        });
})();