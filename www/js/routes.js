(function () {
    angular.module('route', [])  
.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    $ionicConfigProvider.views.maxCache(0);
    $stateProvider.state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'menu.html',
        controller: 'AppCtrl'
    })
    .state('app.feeds', {
        url: '/feeds',
        views: {
            'menuContent': {
                templateUrl: 'feeds.html',
                controller: 'FeedCtrl'
            }        
        }
    })

  .state('app.feeddetail', {
        url: '/FeedDetail/:id',
        views: {
            'menuContent': {
                templateUrl: 'feedDetails.html',
                controller: 'FeedDetailCtrl'
            }
        }
    });
  
    $urlRouterProvider.otherwise('/app/feeds');
});

})();