(function () {
    angular.module('services', [])
        .service('service', ['$http',  function ($http) {

            this.getFeeds = function () {
                return $http({
                    method: "GET",
                    dataType:'JSONP',
                    // url: 'http://pa.tedcdn.com/feeds/talks.rss',
                    url: 'xml.js',
                    crossDomain: true
                })
                    .success(function (response) {
                        console.log(response);
                    })
                    .error(function (error) {
                        console.log(error);
                    });
            };
        }])
        .constant('xmlConstant',{
            xml:''
        });
})();