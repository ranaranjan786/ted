/* global angular, document, window */


angular.module('starter.controllers', [])

    .controller('AppCtrl', ['$scope', '$ionicModal', '$ionicPopover', '$timeout', function ($scope, $ionicModal, $ionicPopover, $timeout) {


        var navIcons = document.getElementsByClassName('ion-navicon');
        for (var i = 0; i < navIcons.length; i++) {
            navIcons.addEventListener('click', function () {
                this.classList.toggle('active');
            });
        }

        ////////////////////////////////////////
        // Layout Methods
        ////////////////////////////////////////

        $scope.hideNavBar = function () {
            document.getElementsByTagName('ion-nav-bar')[0].style.display = 'none';
        };

        $scope.showNavBar = function () {
            document.getElementsByTagName('ion-nav-bar')[0].style.display = 'block';
        };

        $scope.noHeader = function () {
            var content = document.getElementsByTagName('ion-content');
            for (var i = 0; i < content.length; i++) {
                if (content[i].classList.contains('has-header')) {
                    content[i].classList.toggle('has-header');
                }
            }
        };

        $scope.setExpanded = function (bool) {
            $scope.isExpanded = bool;
        };

        $scope.setHeaderFab = function (location) {
            var hasHeaderFabLeft = false;
            var hasHeaderFabRight = false;

            switch (location) {
                case 'left':
                    hasHeaderFabLeft = true;
                    break;
                case 'right':
                    hasHeaderFabRight = true;
                    break;
            }

            $scope.hasHeaderFabLeft = hasHeaderFabLeft;
            $scope.hasHeaderFabRight = hasHeaderFabRight;
        };

        $scope.hasHeader = function () {
            var content = document.getElementsByTagName('ion-content');
            for (var i = 0; i < content.length; i++) {
                if (!content[i].classList.contains('has-header')) {
                    content[i].classList.toggle('has-header');
                }
            }

        };

        $scope.hideHeader = function () {
            $scope.hideNavBar();
            $scope.noHeader();
        };

        $scope.showHeader = function () {
            $scope.showNavBar();
            $scope.hasHeader();
        };

        $scope.clearFabs = function () {
            var fabs = document.getElementsByClassName('button-fab');
            if (fabs.length && fabs.length > 1) {
                fabs[0].remove();
            }
        };
    }])
    .controller('FeedCtrl', ['$scope', '$stateParams', '$timeout', 'ionicMaterialMotion', 'ionicMaterialInk', 'service', 'xmlConstant', 'ngXml2json', '$ionicLoading', '$sce', function ($scope, $stateParams, $timeout, ionicMaterialMotion, ionicMaterialInk, service, xmlConstant, ngXml2json, $ionicLoading, $sce) {

        // We are not calling "https://pa.tedcdn.com/feeds/talks.rss" API because it throws error "No 'Access-Control-Allow-Origin' header is present on the requested resource".
 $ionicLoading.show({ template: 'Please wait' });
        service.getFeeds().then(function (res) {
            // console.log('res', res);
            if (!angular.fromJson(localStorage.getItem("items"))) {
                var jsonObject = ngXml2json.parser(res.data); // converting xml data into json.
                $scope.feeds = jsonObject.rss.channel.item;
                localStorage.setItem("items", angular.toJson($scope.feeds)); // storing feed in localstorage for better performance.
                $ionicLoading.hide();
            }
            else {
               $scope.feeds = angular.fromJson(localStorage.getItem("items")); // get feeds from localstorage if already downladed feeds available.
                $ionicLoading.hide();
              //  $scope.$apply();
            }
        });

       


        $timeout(function () {    // loading hide for 2 second
            $ionicLoading.hide();
        }, 2000);

        $timeout(function () {
            ionicMaterialMotion.fadeSlideIn({
                selector: '.animate-fade-slide-in .item'
            });
        }, 1000);

        // Activate ink for controller
        // ionicMaterialInk.displayEffect();
    }])
    .filter('replaceDash', function () {        // custome filter for replacing  '-' with :.
        return function (url) {
            return url.replace('https-', 'https:');
        };
    })

    .filter('timeFilter', function () {   // custome filter for remove  '00-'.
        return function (time) {
            return time.replace('00-', '');
        };
    })
    .controller('FeedDetailCtrl', ['$scope', '$stateParams', '$timeout', 'ionicMaterialInk', 'ionicMaterialMotion', '$filter', '$sce', function ($scope, $stateParams, $timeout, ionicMaterialInk, ionicMaterialMotion, $filter, $sce) {
        var id = $stateParams.id;   // getting feedID from queryString. 
        var items = angular.fromJson(localStorage.getItem("items"));    // getting feeds from localStorage.
        if (items) {
            $scope.itemObj = $filter('filter')(items, function (item) {
                return id == item.jwplayertalkid;                                   // filter feed details for selected feed.
            })[0];
        }
        $scope.trustSrc = function (url) {
            url = url.replace('https-', 'https:');
            return $sce.trustAsResourceUrl(url);
        };

        $scope.$parent.showHeader();
        $scope.$parent.clearFabs();
        $scope.isExpanded = true;
        $scope.$parent.setExpanded(true);
        $scope.$parent.setHeaderFab(false);

        // Activate ink for controller
        ionicMaterialInk.displayEffect();

        ionicMaterialMotion.pushDown({
            selector: '.push-down'
        });
        ionicMaterialMotion.fadeSlideInRight({
            selector: '.animate-fade-slide-in .item'
        });

    }]);
