<?xml version="1.0" encoding="UTF-8"?>
<rss xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:media="http://search.yahoo.com/mrss/" xmlns:jwplayer="http://developer.longtailvideo.com/" version="2.0">
  <channel>
    <atom:link xmlns:atom10="http://www.w3.org/2005/Atom" rel="self" href="https://pa.tedcdn.com/feeds/talks.rss?1496692261" type="application/rss+xml"/>
    <lastBuildDate>Tue, 06 Jun 2017 08:16:18 +0000</lastBuildDate>
    <title>TEDTalks (video)</title>
    <link>https://www.ted.com/talks</link>
    <generator>TED - TED.com</generator>
    <description>TED is a nonprofit devoted to Ideas Worth Spreading. On this feed, you'll find TEDTalks video to inspire, intrigue and stir the imagination from some of the world's leading thinkers and doers, speaking from the stage at TED conferences, TEDx events and partner events around the world. This podcast is also available in high-def video and audio-only formats.</description>
    <itunes:subtitle>Ideas worth spreading from the TED Conference</itunes:subtitle>
    <itunes:author>TED</itunes:author>
    <itunes:summary>TED is a nonprofit devoted to Ideas Worth Spreading. On this feed, you'll find TEDTalks video to inspire, intrigue and stir the imagination from some of the world's leading thinkers and doers, speaking from the stage at TED conferences, TEDx events and partner events around the world. This podcast is also available in high-def video and audio-only formats.</itunes:summary>
    <language>eng</language>
    <copyright>Creative Commons: https://creativecommons.org/licenses/by-nc-nd/4.0/</copyright>
    <itunes:owner>
      <itunes:name>TED</itunes:name>
      <itunes:email>contact@ted.com</itunes:email>
    </itunes:owner>
    <image>
      <url>https://pl.tedcdn.com/storage_ted_com/TEDTalksvideo_tile_144.jpg</url>
      <title>TEDTalks (video)</title>
      <link>https://www.ted.com/talks</link>
      <width>144</width>
      <height>144</height>
    </image>
    <itunes:image href="https://pc.tedcdn.com/assets/images/itunes/podcast_poster_600x600.jpg"/>
    <category>Science</category>
    <category>Technology</category>
    <category>Entertainment</category>
    <category>Design</category>
    <itunes:category text="Arts">
      <itunes:category text="Design"/>
    </itunes:category>
    <itunes:category text="Education">
      <itunes:category text="Education"/>
    </itunes:category>
    <itunes:category text="Science &amp; Medicine">
      <itunes:category text="Natural Sciences"/>
    </itunes:category>
    <itunes:category text="Technology"/>
    <itunes:keywords>TED, TEDTalks, TED Talks, inspiration, creativity, tech demo, education</itunes:keywords>
    <itunes:explicit>no</itunes:explicit>
    <media:rating scheme="urn:simple">nonadult</media:rating>
    <item>
      <title>How to see past your own perspective and find truth | Michael Patrick Lynch</title>
      <itunes:author>Michael Patrick Lynch</itunes:author>
      <description>
        <![CDATA[The more we read and watch online, the harder it becomes to tell the difference between what's real and what's fake. It's as if we know more but understand less, says philosopher Michael Patrick Lynch. In this talk, he dares us to take active steps to burst our filter bubbles and participate in the common reality that actually underpins everything.]]>
      </description>
      <itunes:subtitle>How to see past your own perspective and find truth | Michael Patrick Lynch</itunes:subtitle>
      <itunes:summary>
        <![CDATA[The more we read and watch online, the harder it becomes to tell the difference between what's real and what's fake. It's as if we know more but understand less, says philosopher Michael Patrick Lynch. In this talk, he dares us to take active steps to burst our filter bubbles and participate in the common reality that actually underpins everything.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/MichaelPatrickLynch_2017.mp4?apikey=TEDRSS" length="49430331"/>
      <link>https://www.ted.com/talks/michael_patrick_lynch_how_to_see_past_your_own_perspective_and_find_truth?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2796</guid>
      <jwplayer:talkId>2796</jwplayer:talkId>
      <pubDate>Mon, 05 Jun 2017 14:59:55 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:14:26</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/MichaelPatrickLynch_2017.mp4?apikey=TEDRSS" fileSize="49430331" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/46ca6ed53d1e86a77af23776f5c693e2cada1b4a_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/46ca6ed53d1e86a77af23776f5c693e2cada1b4a_2880x1620.jpg?"/>
    </item>
    <item>
      <title>How to design a library that makes kids want to read  | Michael Bierut</title>
      <itunes:author>Michael Bierut</itunes:author>
      <description>
        <![CDATA[When Michael Bierut was tapped to design a logo for public school libraries, he had no idea that he was embarking on a years-long passion project. In this often hilarious talk, he recalls his obsessive quest to bring energy, learning, art and graphics into these magical spaces where school librarians can inspire new generations of readers and thinkers.]]>
      </description>
      <itunes:subtitle>How to design a library that makes kids want to read  | Michael Bierut</itunes:subtitle>
      <itunes:summary>
        <![CDATA[When Michael Bierut was tapped to design a logo for public school libraries, he had no idea that he was embarking on a years-long passion project. In this often hilarious talk, he recalls his obsessive quest to bring energy, learning, art and graphics into these magical spaces where school librarians can inspire new generations of readers and thinkers.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/MichaelBierut_2017S.mp4?apikey=TEDRSS" length="42873357"/>
      <link>https://www.ted.com/talks/michael_bierut_how_to_design_a_library_that_makes_kids_want_to_read?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2743</guid>
      <jwplayer:talkId>2743</jwplayer:talkId>
      <pubDate>Fri, 02 Jun 2017 15:40:30 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:12:26</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/MichaelBierut_2017S.mp4?apikey=TEDRSS" fileSize="42873357" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/0964e5ab96e6c42a107be1a09f6ad8c07e3b3775_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/0964e5ab96e6c42a107be1a09f6ad8c07e3b3775_2880x1620.jpg?"/>
    </item>
    <item>
      <title>3 songs that bring history to life | Rhiannon Giddens</title>
      <itunes:author>Rhiannon Giddens</itunes:author>
      <description>
        <![CDATA[Rhiannon Giddens pours the emotional weight of American history into her music. Listen as she performs two traditional folk ballads, "Waterboy" and "Up Above My Head," and one glorious original song, "Come Love Come," inspired by Civil War-era slave narratives.
]]>
      </description>
      <itunes:subtitle>3 songs that bring history to life | Rhiannon Giddens</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Rhiannon Giddens pours the emotional weight of American history into her music. Listen as she performs two traditional folk ballads, "Waterboy" and "Up Above My Head," and one glorious original song, "Come Love Come," inspired by Civil War-era slave narratives.
]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/RhiannonGiddens_2016.mp4?apikey=TEDRSS" length="50527274"/>
      <link>https://www.ted.com/talks/rhiannon_giddens_3_songs_that_bring_history_to_life?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2795</guid>
      <jwplayer:talkId>2795</jwplayer:talkId>
      <pubDate>Fri, 02 Jun 2017 10:54:17 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:14:45</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/RhiannonGiddens_2016.mp4?apikey=TEDRSS" fileSize="50527274" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/386f1663b47894f25894b205258a2b97635c7cd1_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/386f1663b47894f25894b205258a2b97635c7cd1_2880x1620.jpg?"/>
    </item>
    <item>
      <title>No one should die because they live too far from a doctor | Raj Panjabi</title>
      <itunes:author>Raj Panjabi</itunes:author>
      <description>
        <![CDATA[Illness is universal -- but access to care is not. Physician Raj Panjabi has a bold vision to bring health care to everyone, everywhere. With the 2017 TED Prize, Panjabi is building the Community Health Academy, a global platform that aims to modernize how community health workers learn vital skills, creating jobs along the way.]]>
      </description>
      <itunes:subtitle>No one should die because they live too far from a doctor | Raj Panjabi</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Illness is universal -- but access to care is not. Physician Raj Panjabi has a bold vision to bring health care to everyone, everywhere. With the 2017 TED Prize, Panjabi is building the Community Health Academy, a global platform that aims to modernize how community health workers learn vital skills, creating jobs along the way.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/RajPanjabi_2017.mp4?apikey=TEDRSS" length="70097939"/>
      <link>https://www.ted.com/talks/raj_panjabi_no_one_should_die_because_they_live_too_far_from_a_doctor?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2789</guid>
      <jwplayer:talkId>2789</jwplayer:talkId>
      <pubDate>Thu, 01 Jun 2017 14:30:43 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:20:30</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/RajPanjabi_2017.mp4?apikey=TEDRSS" fileSize="70097939" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/dbb016fcbd1b89543bb9229ba73f03e89c51aa7a_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/dbb016fcbd1b89543bb9229ba73f03e89c51aa7a_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Am I not human? A call for criminal justice reform | Marlon Peterson</title>
      <itunes:author>Marlon Peterson</itunes:author>
      <description>
        <![CDATA[For a crime he committed in his early twenties, the courts sentenced Marlon Peterson to 10 years in prison -- and, as he says, a lifetime of irrelevance. While behind bars, Peterson found redemption through a penpal mentorship program with students from Brooklyn. In this brave talk, he reminds us why we should invest in the humanity of those people society would like to disregard and discard.]]>
      </description>
      <itunes:subtitle>Am I not human? A call for criminal justice reform | Marlon Peterson</itunes:subtitle>
      <itunes:summary>
        <![CDATA[For a crime he committed in his early twenties, the courts sentenced Marlon Peterson to 10 years in prison -- and, as he says, a lifetime of irrelevance. While behind bars, Peterson found redemption through a penpal mentorship program with students from Brooklyn. In this brave talk, he reminds us why we should invest in the humanity of those people society would like to disregard and discard.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/MarlonPeterson_2016S.mp4?apikey=TEDRSS" length="26024362"/>
      <link>https://www.ted.com/talks/marlon_peterson_am_i_not_human_a_call_for_criminal_justice_reform?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2794</guid>
      <jwplayer:talkId>2794</jwplayer:talkId>
      <pubDate>Wed, 31 May 2017 14:48:12 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:07:32</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/MarlonPeterson_2016S.mp4?apikey=TEDRSS" fileSize="26024362" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/9e09d0fa896ca879988163bbb2f490109dac3d2f_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/9e09d0fa896ca879988163bbb2f490109dac3d2f_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Don't fear intelligent machines. Work with them | Garry Kasparov</title>
      <itunes:author>Garry Kasparov</itunes:author>
      <description>
        <![CDATA[We must face our fears if we want to get the most out of technology -- and we must conquer those fears if we want to get the best out of humanity, says Garry Kasparov. One of the greatest chess players in history, Kasparov lost a memorable match to IBM supercomputer Deep Blue in 1997. Now he shares his vision for a future where intelligent machines help us turn our grandest dreams into reality.]]>
      </description>
      <itunes:subtitle>Don't fear intelligent machines. Work with them | Garry Kasparov</itunes:subtitle>
      <itunes:summary>
        <![CDATA[We must face our fears if we want to get the most out of technology -- and we must conquer those fears if we want to get the best out of humanity, says Garry Kasparov. One of the greatest chess players in history, Kasparov lost a memorable match to IBM supercomputer Deep Blue in 1997. Now he shares his vision for a future where intelligent machines help us turn our grandest dreams into reality.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/GarryKasparov_2017.mp4?apikey=TEDRSS" length="52512007"/>
      <link>https://www.ted.com/talks/garry_kasparov_don_t_fear_intelligent_machines_work_with_them?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2787</guid>
      <jwplayer:talkId>2787</jwplayer:talkId>
      <pubDate>Tue, 30 May 2017 14:59:59 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:15:20</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/GarryKasparov_2017.mp4?apikey=TEDRSS" fileSize="52512007" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/0d28c4301c3c7ed0a3790e31231e71e1469524e6_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/0d28c4301c3c7ed0a3790e31231e71e1469524e6_2880x1620.jpg?"/>
    </item>
    <item>
      <title>How pollution is changing the ocean's chemistry | Triona McGrath</title>
      <itunes:author>Triona McGrath</itunes:author>
      <description>
        <![CDATA[As we keep pumping carbon dioxide into the atmosphere, more of it is dissolving in the oceans, leading to drastic changes in the water's chemistry. Triona McGrath researches this process, known as ocean acidification, and in this talk she takes us for a dive into an oceanographer's world. Learn more about how the "evil twin of climate change" is impacting the ocean -- and the life that depends on it.]]>
      </description>
      <itunes:subtitle>How pollution is changing the ocean's chemistry | Triona McGrath</itunes:subtitle>
      <itunes:summary>
        <![CDATA[As we keep pumping carbon dioxide into the atmosphere, more of it is dissolving in the oceans, leading to drastic changes in the water's chemistry. Triona McGrath researches this process, known as ocean acidification, and in this talk she takes us for a dive into an oceanographer's world. Learn more about how the "evil twin of climate change" is impacting the ocean -- and the life that depends on it.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/TrionaMcGrath_2016X.mp4?apikey=TEDRSS" length="31462667"/>
      <link>https://www.ted.com/talks/triona_mcgrath_how_pollution_is_changing_the_ocean_s_chemistry?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2782</guid>
      <jwplayer:talkId>2782</jwplayer:talkId>
      <pubDate>Mon, 29 May 2017 15:03:18 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:09:03</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/TrionaMcGrath_2016X.mp4?apikey=TEDRSS" fileSize="31462667" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/49ba3b74fca20ba8af6b129652f39b84d10e6ff8_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/49ba3b74fca20ba8af6b129652f39b84d10e6ff8_2880x1620.jpg?"/>
    </item>
    <item>
      <title>How to find a wonderful idea |  OK Go</title>
      <itunes:author> OK Go</itunes:author>
      <description>
        <![CDATA[Where does OK Go come up with ideas like dancing in zero gravity, performing in ultra slow motion or constructing a warehouse-sized Rube Goldberg machine for their music videos? In between live performances of "This Too Shall Pass" and "The One Moment," lead singer and director Damian Kulash takes us inside the band's creative process, showing us how to look for wonder and surprise.]]>
      </description>
      <itunes:subtitle>How to find a wonderful idea |  OK Go</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Where does OK Go come up with ideas like dancing in zero gravity, performing in ultra slow motion or constructing a warehouse-sized Rube Goldberg machine for their music videos? In between live performances of "This Too Shall Pass" and "The One Moment," lead singer and director Damian Kulash takes us inside the band's creative process, showing us how to look for wonder and surprise.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/OKGo_2017.mp4?apikey=TEDRSS" length="60181084"/>
      <link>https://www.ted.com/talks/ok_go_how_to_find_a_wonderful_idea?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2792</guid>
      <jwplayer:talkId>2792</jwplayer:talkId>
      <pubDate>Fri, 26 May 2017 14:49:27 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:17:35</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/OKGo_2017.mp4?apikey=TEDRSS" fileSize="60181084" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/5558298f5b2a0211b3c8de807b632ab2eca3d769_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/5558298f5b2a0211b3c8de807b632ab2eca3d769_2880x1620.jpg?"/>
    </item>
    <item>
      <title>A secret weapon against Zika and other mosquito-borne diseases | Nina Fedoroff</title>
      <itunes:author>Nina Fedoroff</itunes:author>
      <description>
        <![CDATA[Where did Zika come from, and what can we do about it? Molecular biologist Nina Fedoroff takes us around the world to understand Zika's origins and how it spread, proposing a controversial way to stop the virus -- and other deadly diseases -- by preventing infected mosquitoes from multiplying.]]>
      </description>
      <itunes:subtitle>A secret weapon against Zika and other mosquito-borne diseases | Nina Fedoroff</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Where did Zika come from, and what can we do about it? Molecular biologist Nina Fedoroff takes us around the world to understand Zika's origins and how it spread, proposing a controversial way to stop the virus -- and other deadly diseases -- by preventing infected mosquitoes from multiplying.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/NinaFedoroff_2016X.mp4?apikey=TEDRSS" length="52210925"/>
      <link>https://www.ted.com/talks/nina_fedoroff_a_secret_weapon_against_zika_and_other_mosquito_borne_diseases?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2793</guid>
      <jwplayer:talkId>2793</jwplayer:talkId>
      <pubDate>Thu, 25 May 2017 14:53:13 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:15:10</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/NinaFedoroff_2016X.mp4?apikey=TEDRSS" fileSize="52210925" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/8983c0874eebad4f66246624c3d65aacd5952409_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/8983c0874eebad4f66246624c3d65aacd5952409_2880x1620.jpg?"/>
    </item>
    <item>
      <title>This is what democracy looks like | Anthony D. Romero</title>
      <itunes:author>Anthony D. Romero</itunes:author>
      <description>
        <![CDATA[In a quest to make sense of the political environment in the United States in 2017, lawyer and ACLU executive director Anthony D. Romero turned to a surprising place -- a 14th-century fresco by Italian Renaissance master Ambrogio Lorenzetti. What could a 700-year-old painting possibly teach us about life today? Turns out, a lot. Romero explains all in a talk that's as striking as the painting itself.]]>
      </description>
      <itunes:subtitle>This is what democracy looks like | Anthony D. Romero</itunes:subtitle>
      <itunes:summary>
        <![CDATA[In a quest to make sense of the political environment in the United States in 2017, lawyer and ACLU executive director Anthony D. Romero turned to a surprising place -- a 14th-century fresco by Italian Renaissance master Ambrogio Lorenzetti. What could a 700-year-old painting possibly teach us about life today? Turns out, a lot. Romero explains all in a talk that's as striking as the painting itself.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/AnthonyDRomero_2017.mp4?apikey=TEDRSS" length="43836231"/>
      <link>https://www.ted.com/talks/anthony_d_romero_this_is_what_democracy_looks_like?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2791</guid>
      <jwplayer:talkId>2791</jwplayer:talkId>
      <pubDate>Wed, 24 May 2017 14:51:03 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:12:48</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/AnthonyDRomero_2017.mp4?apikey=TEDRSS" fileSize="43836231" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/356a95fda707e54644ae9416e9c2365b9f17ddbd_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/356a95fda707e54644ae9416e9c2365b9f17ddbd_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Why I speak up about living with epilepsy | Sitawa Wafula</title>
      <itunes:author>Sitawa Wafula</itunes:author>
      <description>
        <![CDATA[Once homebound by epilepsy, mental health advocate Sitawa Wafula found her strength in writing about it. Now, she advocates for others who are yet to find their voices, cutting through stigma and exclusion to talk about what it's like to live with the condition.]]>
      </description>
      <itunes:subtitle>Why I speak up about living with epilepsy | Sitawa Wafula</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Once homebound by epilepsy, mental health advocate Sitawa Wafula found her strength in writing about it. Now, she advocates for others who are yet to find their voices, cutting through stigma and exclusion to talk about what it's like to live with the condition.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/SitawaWafula_2017S.mp4?apikey=TEDRSS" length="29589847"/>
      <link>https://www.ted.com/talks/sitawa_wafula_why_i_speak_up_about_living_with_epilepsy?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2786</guid>
      <jwplayer:talkId>2786</jwplayer:talkId>
      <pubDate>Tue, 23 May 2017 15:06:46 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:08:29</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/SitawaWafula_2017S.mp4?apikey=TEDRSS" fileSize="29589847" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/2c8b48d4d480f4441742ba6d2f9bf0b9abfd906e_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/2c8b48d4d480f4441742ba6d2f9bf0b9abfd906e_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Poverty isn't a lack of character; it's a lack of cash | Rutger Bregman</title>
      <itunes:author>Rutger Bregman</itunes:author>
      <description>
        <![CDATA["Ideas can and do change the world," says historian Rutger Bregman, sharing his case for a provocative one: guaranteed basic income. Learn more about the idea's 500-year history and a forgotten modern experiment where it actually worked -- and imagine how much energy and talent we would unleash if we got rid of poverty once and for all.]]>
      </description>
      <itunes:subtitle>Poverty isn't a lack of character; it's a lack of cash | Rutger Bregman</itunes:subtitle>
      <itunes:summary>
        <![CDATA["Ideas can and do change the world," says historian Rutger Bregman, sharing his case for a provocative one: guaranteed basic income. Learn more about the idea's 500-year history and a forgotten modern experiment where it actually worked -- and imagine how much energy and talent we would unleash if we got rid of poverty once and for all.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/RutgerBregman_2017.mp4?apikey=TEDRSS" length="51287057"/>
      <link>https://www.ted.com/talks/rutger_bregman_poverty_isn_t_a_lack_of_character_it_s_a_lack_of_cash?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2785</guid>
      <jwplayer:talkId>2785</jwplayer:talkId>
      <pubDate>Mon, 22 May 2017 15:06:14 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:14:58</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/RutgerBregman_2017.mp4?apikey=TEDRSS" fileSize="51287057" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/3b8c0da5a6ab101fbdc84071c08ba81cf78ef2ec_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/3b8c0da5a6ab101fbdc84071c08ba81cf78ef2ec_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Walking as a revolutionary act of self-care | T. Morgan Dixon and Vanessa Garrison</title>
      <itunes:author>T. Morgan Dixon and Vanessa Garrison</itunes:author>
      <description>
        <![CDATA["When black women walk, things change," say T. Morgan Dixon and Vanessa Garrison, the founders of the health nonprofit GirlTrek. They're on a mission to reduce the leading causes of preventable death among black women -- and build communities in the process. How? By getting one million black women and girls to prioritize their self-care, lacing up their shoes and walking in the direction of their healthiest, most fulfilled lives.]]>
      </description>
      <itunes:subtitle>Walking as a revolutionary act of self-care | T. Morgan Dixon and Vanessa Garrison</itunes:subtitle>
      <itunes:summary>
        <![CDATA["When black women walk, things change," say T. Morgan Dixon and Vanessa Garrison, the founders of the health nonprofit GirlTrek. They're on a mission to reduce the leading causes of preventable death among black women -- and build communities in the process. How? By getting one million black women and girls to prioritize their self-care, lacing up their shoes and walking in the direction of their healthiest, most fulfilled lives.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/TMorganDixonandVanessaGarrison_2017.mp4?apikey=TEDRSS" length="53271098"/>
      <link>https://www.ted.com/talks/t_morgan_dixon_and_vanessa_garrison_walking_as_a_revolutionary_act_of_self_care?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2788</guid>
      <jwplayer:talkId>2788</jwplayer:talkId>
      <pubDate>Fri, 19 May 2017 15:03:52 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:15:33</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/TMorganDixonandVanessaGarrison_2017.mp4?apikey=TEDRSS" fileSize="53271098" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/45ab201f65e1825771f3f3a310088588e5c344e6_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/45ab201f65e1825771f3f3a310088588e5c344e6_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Why school should start later for teens | Wendy Troxel</title>
      <itunes:author>Wendy Troxel</itunes:author>
      <description>
        <![CDATA[Teens don't get enough sleep, and it's not because of Snapchat, social lives or hormones -- it's because of public policy, says Wendy Troxel. Drawing from her experience as a sleep researcher, clinician and mother of a teenager, Troxel discusses how early school start times deprive adolescents of sleep during the time of their lives when they need it most.]]>
      </description>
      <itunes:subtitle>Why school should start later for teens | Wendy Troxel</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Teens don't get enough sleep, and it's not because of Snapchat, social lives or hormones -- it's because of public policy, says Wendy Troxel. Drawing from her experience as a sleep researcher, clinician and mother of a teenager, Troxel discusses how early school start times deprive adolescents of sleep during the time of their lives when they need it most.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/WendyTroxel_2016X.mp4?apikey=TEDRSS" length="36474027"/>
      <link>https://www.ted.com/talks/wendy_troxel_why_school_should_start_later_for_teens?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2783</guid>
      <jwplayer:talkId>2783</jwplayer:talkId>
      <pubDate>Thu, 18 May 2017 14:58:38 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:10:33</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/WendyTroxel_2016X.mp4?apikey=TEDRSS" fileSize="36474027" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/e3acf448fa6bcb48cad60eaf10306432a0f5b273_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/e3acf448fa6bcb48cad60eaf10306432a0f5b273_2880x1620.jpg?"/>
    </item>
    <item>
      <title>A climate solution where all sides can win | Ted Halstead</title>
      <itunes:author>Ted Halstead</itunes:author>
      <description>
        <![CDATA[Why are we so deadlocked on climate, and what would it take to overcome the seemingly insurmountable barriers to progress? Policy entrepreneur Ted Halstead proposes a transformative solution based on the conservative principles of free markets and limited government. Learn more about how this carbon dividends plan could trigger an international domino effect towards a more popular, cost-effective and equitable climate solution.]]>
      </description>
      <itunes:subtitle>A climate solution where all sides can win | Ted Halstead</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Why are we so deadlocked on climate, and what would it take to overcome the seemingly insurmountable barriers to progress? Policy entrepreneur Ted Halstead proposes a transformative solution based on the conservative principles of free markets and limited government. Learn more about how this carbon dividends plan could trigger an international domino effect towards a more popular, cost-effective and equitable climate solution.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/TedHalstead_2017.mp4?apikey=TEDRSS" length="45058553"/>
      <link>https://www.ted.com/talks/ted_halstead_a_climate_solution_where_all_sides_can_win?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2784</guid>
      <jwplayer:talkId>2784</jwplayer:talkId>
      <pubDate>Wed, 17 May 2017 14:38:20 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:13:07</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/TedHalstead_2017.mp4?apikey=TEDRSS" fileSize="45058553" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/e835e670a7836cf65aca2a7a644fd94398cb4b8e_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/e835e670a7836cf65aca2a7a644fd94398cb4b8e_2880x1620.jpg?"/>
    </item>
    <item>
      <title>What makes life worth living in the face of death | Lucy Kalanithi</title>
      <itunes:author>Lucy Kalanithi</itunes:author>
      <description>
        <![CDATA[In this deeply moving talk, Lucy Kalanithi reflects on life and purpose, sharing the story of her late husband, Paul, a young neurosurgeon who turned to writing after his terminal cancer diagnosis. "Engaging in the full range of experience -- living and dying, love and loss -- is what we get to do," Kalanithi says. "Being human doesn't happen despite suffering -- it happens within it."]]>
      </description>
      <itunes:subtitle>What makes life worth living in the face of death | Lucy Kalanithi</itunes:subtitle>
      <itunes:summary>
        <![CDATA[In this deeply moving talk, Lucy Kalanithi reflects on life and purpose, sharing the story of her late husband, Paul, a young neurosurgeon who turned to writing after his terminal cancer diagnosis. "Engaging in the full range of experience -- living and dying, love and loss -- is what we get to do," Kalanithi says. "Being human doesn't happen despite suffering -- it happens within it."]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/LucyKalanithi_2016P.mp4?apikey=TEDRSS" length="55300064"/>
      <link>https://www.ted.com/talks/lucy_kalanithi_what_makes_life_worth_living_in_the_face_of_death?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2778</guid>
      <jwplayer:talkId>2778</jwplayer:talkId>
      <pubDate>Tue, 16 May 2017 15:01:03 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:16:09</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/LucyKalanithi_2016P.mp4?apikey=TEDRSS" fileSize="55300064" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/d13cde084b20383f6de5a5e49f5ff459d3d05c88_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/d13cde084b20383f6de5a5e49f5ff459d3d05c88_2880x1620.jpg?"/>
    </item>
    <item>
      <title>3 principles for creating safer AI | Stuart Russell</title>
      <itunes:author>Stuart Russell</itunes:author>
      <description>
        <![CDATA[How can we harness the power of superintelligent AI while also preventing the catastrophe of robotic takeover? As we move closer toward creating all-knowing machines, AI pioneer Stuart Russell is working on something a bit different: robots with uncertainty. Hear his vision for human-compatible AI that can solve problems using common sense, altruism and other human values.]]>
      </description>
      <itunes:subtitle>3 principles for creating safer AI | Stuart Russell</itunes:subtitle>
      <itunes:summary>
        <![CDATA[How can we harness the power of superintelligent AI while also preventing the catastrophe of robotic takeover? As we move closer toward creating all-knowing machines, AI pioneer Stuart Russell is working on something a bit different: robots with uncertainty. Hear his vision for human-compatible AI that can solve problems using common sense, altruism and other human values.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/StuartRussell_2017.mp4?apikey=TEDRSS" length="60210048"/>
      <link>https://www.ted.com/talks/stuart_russell_how_ai_might_make_us_better_people?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2781</guid>
      <jwplayer:talkId>2781</jwplayer:talkId>
      <pubDate>Mon, 15 May 2017 14:29:46 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:17:35</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/StuartRussell_2017.mp4?apikey=TEDRSS" fileSize="60210048" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/3f614c0a546467f66777c847998104c6f185fe99_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/3f614c0a546467f66777c847998104c6f185fe99_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Thoughts on humanity, fame and love | Shah Rukh Khan</title>
      <itunes:author>Shah Rukh Khan</itunes:author>
      <description>
        <![CDATA["I sell dreams, and I peddle love to millions of people," says Shah Rukh Khan, Bollywood's biggest star. In this charming, funny talk, Khan traces the arc of his life, showcases a few of his famous dance moves and shares hard-earned wisdom from a life spent in the spotlight.]]>
      </description>
      <itunes:subtitle>Thoughts on humanity, fame and love | Shah Rukh Khan</itunes:subtitle>
      <itunes:summary>
        <![CDATA["I sell dreams, and I peddle love to millions of people," says Shah Rukh Khan, Bollywood's biggest star. In this charming, funny talk, Khan traces the arc of his life, showcases a few of his famous dance moves and shares hard-earned wisdom from a life spent in the spotlight.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/ShahRukhKhan_2017.mp4?apikey=TEDRSS" length="61269074"/>
      <link>https://www.ted.com/talks/shah_rukh_khan_thoughts_on_humanity_fame_and_love?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2780</guid>
      <jwplayer:talkId>2780</jwplayer:talkId>
      <pubDate>Fri, 12 May 2017 19:44:40 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:17:51</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/ShahRukhKhan_2017.mp4?apikey=TEDRSS" fileSize="61269074" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/feccecc42f676b35b9aa1d0832eb1185ea8eb285_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/feccecc42f676b35b9aa1d0832eb1185ea8eb285_2880x1620.jpg?"/>
    </item>
    <item>
      <title>How human noise affects ocean habitats | Kate Stafford</title>
      <itunes:author>Kate Stafford</itunes:author>
      <description>
        <![CDATA[Oceanographer Kate Stafford lowers us into the sonically rich depths of the Arctic Ocean, where ice groans, whales sing to communicate over vast distances -- and climate change and human noise threaten to alter the environment in ways we don't understand. Learn more about why this underwater soundscape matters and what we might do to protect it.]]>
      </description>
      <itunes:subtitle>How human noise affects ocean habitats | Kate Stafford</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Oceanographer Kate Stafford lowers us into the sonically rich depths of the Arctic Ocean, where ice groans, whales sing to communicate over vast distances -- and climate change and human noise threaten to alter the environment in ways we don't understand. Learn more about why this underwater soundscape matters and what we might do to protect it.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/KateStafford_2016X.mp4?apikey=TEDRSS" length="40890735"/>
      <link>https://www.ted.com/talks/kate_stafford_how_human_noise_affects_ocean_habitats?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2776</guid>
      <jwplayer:talkId>2776</jwplayer:talkId>
      <pubDate>Fri, 12 May 2017 15:04:35 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:11:51</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/KateStafford_2016X.mp4?apikey=TEDRSS" fileSize="40890735" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/27b4e032054a41fb1be7b310312f97feee75cb02_1920x1080.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/27b4e032054a41fb1be7b310312f97feee75cb02_1920x1080.jpg?"/>
    </item>
    <item>
      <title>Poetry, music and identity | Jorge Drexler</title>
      <itunes:author>Jorge Drexler</itunes:author>
      <description>
        <![CDATA[One night in 2002, a friend gave Jorge Drexler the chorus to a song and challenged him to write the rest of it using a complex, poetic form known as the "Décima." In this fascinating talk, Drexler examines the blended nature of identity, weaving together the history of the Décima with his own quest to write one. He closes the talk with a performance of the resulting song, "La Milonga del Moro Judío." (In Spanish with English subtitles)]]>
      </description>
      <itunes:subtitle>Poetry, music and identity | Jorge Drexler</itunes:subtitle>
      <itunes:summary>
        <![CDATA[One night in 2002, a friend gave Jorge Drexler the chorus to a song and challenged him to write the rest of it using a complex, poetic form known as the "Décima." In this fascinating talk, Drexler examines the blended nature of identity, weaving together the history of the Décima with his own quest to write one. He closes the talk with a performance of the resulting song, "La Milonga del Moro Judío." (In Spanish with English subtitles)]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/JorgeDrexler_2017U.mp4?apikey=TEDRSS" length="57007568"/>
      <link>https://www.ted.com/talks/jorge_drexler_poetry_music_and_identity?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2777</guid>
      <jwplayer:talkId>2777</jwplayer:talkId>
      <pubDate>Wed, 10 May 2017 14:46:18 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:16:40</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/JorgeDrexler_2017U.mp4?apikey=TEDRSS" fileSize="57007568" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/64d111cddc2a72e2d44ff5b3fe340a48ebd6bfca_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/64d111cddc2a72e2d44ff5b3fe340a48ebd6bfca_2880x1620.jpg?"/>
    </item>
    <item>
      <title>The biology of our best and worst selves | Robert Sapolsky</title>
      <itunes:author>Robert Sapolsky</itunes:author>
      <description>
        <![CDATA[How can humans be so compassionate and altruistic -- and also so brutal and violent? To understand why we do what we do, neuroscientist Robert Sapolsky looks at extreme context, examining actions on timescales from seconds to millions of years before they occurred. In this fascinating talk, he shares his cutting edge research into the biology that drives our worst and best behaviors.]]>
      </description>
      <itunes:subtitle>The biology of our best and worst selves | Robert Sapolsky</itunes:subtitle>
      <itunes:summary>
        <![CDATA[How can humans be so compassionate and altruistic -- and also so brutal and violent? To understand why we do what we do, neuroscientist Robert Sapolsky looks at extreme context, examining actions on timescales from seconds to millions of years before they occurred. In this fascinating talk, he shares his cutting edge research into the biology that drives our worst and best behaviors.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/RobertSapolsky_2017.mp4?apikey=TEDRSS" length="54206065"/>
      <link>https://www.ted.com/talks/robert_sapolsky_the_biology_of_our_best_and_worst_selves?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2779</guid>
      <jwplayer:talkId>2779</jwplayer:talkId>
      <pubDate>Tue, 09 May 2017 15:04:28 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:15:51</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/RobertSapolsky_2017.mp4?apikey=TEDRSS" fileSize="54206065" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/b26fe51ea21f64fe83ae103bf153d49a20ab4fc6_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/b26fe51ea21f64fe83ae103bf153d49a20ab4fc6_2880x1620.jpg?"/>
    </item>
    <item>
      <title>A tribute to nurses | Carolyn Jones</title>
      <itunes:author>Carolyn Jones</itunes:author>
      <description>
        <![CDATA[Carolyn Jones spent five years interviewing, photographing and filming nurses across America, traveling to places dealing with some of the nation's biggest public health issues. She shares personal stories of unwavering dedication in this celebration of the everyday heroes who work at the front lines of health care.]]>
      </description>
      <itunes:subtitle>A tribute to nurses | Carolyn Jones</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Carolyn Jones spent five years interviewing, photographing and filming nurses across America, traveling to places dealing with some of the nation's biggest public health issues. She shares personal stories of unwavering dedication in this celebration of the everyday heroes who work at the front lines of health care.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/CarolynJones_2016P.mp4?apikey=TEDRSS" length="37210450"/>
      <link>https://www.ted.com/talks/carolyn_jones_a_tribute_to_nurses?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2742</guid>
      <jwplayer:talkId>2742</jwplayer:talkId>
      <pubDate>Mon, 08 May 2017 15:02:41 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:10:48</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/CarolynJones_2016P.mp4?apikey=TEDRSS" fileSize="37210450" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/89ce7a4099e5f95833a083f167071c7e70e75ecd_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/89ce7a4099e5f95833a083f167071c7e70e75ecd_2880x1620.jpg?"/>
    </item>
    <item>
      <title>A summer school kids actually want to attend | Karim Abouelnaga</title>
      <itunes:author>Karim Abouelnaga</itunes:author>
      <description>
        <![CDATA[In the US, most kids have a very long summer break, during which they forget an awful lot of what they learned during the school year. This "summer slump" affects kids from low-income neighborhoods most, setting them back almost three months. TED Fellow Karim Abouelnaga has a plan to reverse this learning loss. Learn how he's helping kids improve their chances for a brighter future.]]>
      </description>
      <itunes:subtitle>A summer school kids actually want to attend | Karim Abouelnaga</itunes:subtitle>
      <itunes:summary>
        <![CDATA[In the US, most kids have a very long summer break, during which they forget an awful lot of what they learned during the school year. This "summer slump" affects kids from low-income neighborhoods most, setting them back almost three months. TED Fellow Karim Abouelnaga has a plan to reverse this learning loss. Learn how he's helping kids improve their chances for a brighter future.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/KarimAbouelnaga_2017U.mp4?apikey=TEDRSS" length="24572923"/>
      <link>https://www.ted.com/talks/karim_abouelnaga_a_summer_school_kids_actually_want_to_attend?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2775</guid>
      <jwplayer:talkId>2775</jwplayer:talkId>
      <pubDate>Fri, 05 May 2017 14:48:59 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:07:05</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/KarimAbouelnaga_2017U.mp4?apikey=TEDRSS" fileSize="24572923" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/74d96d8e697a639b075e2fb5fe68d53a9718ef86_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/74d96d8e697a639b075e2fb5fe68d53a9718ef86_2880x1620.jpg?"/>
    </item>
    <item>
      <title>There's no shame in taking care of your mental health | Sangu Delle</title>
      <itunes:author>Sangu Delle</itunes:author>
      <description>
        <![CDATA[When stress got to be too much for TED Fellow Sangu Delle, he had to confront his own deep prejudice: that men shouldn't take care of their mental health. In a personal talk, Delle shares how he learned to handle anxiety in a society that's uncomfortable with emotions. As he says: "Being honest about how we feel doesn't make us weak -- it makes us human."]]>
      </description>
      <itunes:subtitle>There's no shame in taking care of your mental health | Sangu Delle</itunes:subtitle>
      <itunes:summary>
        <![CDATA[When stress got to be too much for TED Fellow Sangu Delle, he had to confront his own deep prejudice: that men shouldn't take care of their mental health. In a personal talk, Delle shares how he learned to handle anxiety in a society that's uncomfortable with emotions. As he says: "Being honest about how we feel doesn't make us weak -- it makes us human."]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/SanguDelle_2017S.mp4?apikey=TEDRSS" length="31435733"/>
      <link>https://www.ted.com/talks/sangu_delle_there_s_no_shame_in_taking_care_of_your_mental_health?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2739</guid>
      <jwplayer:talkId>2739</jwplayer:talkId>
      <pubDate>Thu, 04 May 2017 14:57:04 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:09:06</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/SanguDelle_2017S.mp4?apikey=TEDRSS" fileSize="31435733" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/633a1852f14a705ea7611654c34825f631be1d6e_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/633a1852f14a705ea7611654c34825f631be1d6e_2880x1620.jpg?"/>
    </item>
    <item>
      <title>How to exploit democracy | Laura Galante</title>
      <itunes:author>Laura Galante</itunes:author>
      <description>
        <![CDATA[Hacking, fake news, information bubbles ... all these and more have become part of the vernacular in recent years. But as cyberspace analyst Laura Galante describes in this alarming talk, the real target of anyone looking to influence geopolitics is dastardly simple: it's you.]]>
      </description>
      <itunes:subtitle>How to exploit democracy | Laura Galante</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Hacking, fake news, information bubbles ... all these and more have become part of the vernacular in recent years. But as cyberspace analyst Laura Galante describes in this alarming talk, the real target of anyone looking to influence geopolitics is dastardly simple: it's you.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/LauraGalante_2017.mp4?apikey=TEDRSS" length="32895707"/>
      <link>https://www.ted.com/talks/laura_galante_how_to_exploit_democracy?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2772</guid>
      <jwplayer:talkId>2772</jwplayer:talkId>
      <pubDate>Wed, 03 May 2017 14:56:04 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:09:33</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/LauraGalante_2017.mp4?apikey=TEDRSS" fileSize="32895707" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/5261c8973a6f8df925370ea097cce0271bdc329d_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/5261c8973a6f8df925370ea097cce0271bdc329d_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Behind the lies of Holocaust denial | Deborah Lipstadt</title>
      <itunes:author>Deborah Lipstadt</itunes:author>
      <description>
        <![CDATA["There are facts, there are opinions, and there are lies," says historian Deborah Lipstadt, telling the remarkable story of her research into Holocaust deniers -- and their deliberate distortion of history. Lipstadt encourages us all to go on the offensive against those who assault the truth and facts. "Truth is not relative," she says.]]>
      </description>
      <itunes:subtitle>Behind the lies of Holocaust denial | Deborah Lipstadt</itunes:subtitle>
      <itunes:summary>
        <![CDATA["There are facts, there are opinions, and there are lies," says historian Deborah Lipstadt, telling the remarkable story of her research into Holocaust deniers -- and their deliberate distortion of history. Lipstadt encourages us all to go on the offensive against those who assault the truth and facts. "Truth is not relative," she says.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/DeborahLipstadt_2017X.mp4?apikey=TEDRSS" length="53292835"/>
      <link>https://www.ted.com/talks/deborah_lipstadt_behind_the_lies_of_holocaust_denial?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2769</guid>
      <jwplayer:talkId>2769</jwplayer:talkId>
      <pubDate>Tue, 02 May 2017 15:15:50 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:15:30</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/DeborahLipstadt_2017X.mp4?apikey=TEDRSS" fileSize="53292835" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/cbd0f0b2e7d46c31e538cfe40f7b0a1c0c2a887e_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/cbd0f0b2e7d46c31e538cfe40f7b0a1c0c2a887e_2880x1620.jpg?"/>
    </item>
    <item>
      <title>The future we're building -- and boring | Elon Musk</title>
      <itunes:author>Elon Musk</itunes:author>
      <description>
        <![CDATA[Elon Musk discusses his new project digging tunnels under LA, the latest from Tesla and SpaceX and his motivation for building a future on Mars in conversation with TED's Head Curator, Chris Anderson.]]>
      </description>
      <itunes:subtitle>The future we're building -- and boring | Elon Musk</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Elon Musk discusses his new project digging tunnels under LA, the latest from Tesla and SpaceX and his motivation for building a future on Mars in conversation with TED's Head Curator, Chris Anderson.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/ElonMusk_2017.mp4?apikey=TEDRSS" length="139179224"/>
      <link>https://www.ted.com/talks/elon_musk_the_future_we_re_building_and_boring?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2774</guid>
      <jwplayer:talkId>2774</jwplayer:talkId>
      <pubDate>Sun, 30 Apr 2017 23:58:03 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:40:50</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/ElonMusk_2017.mp4?apikey=TEDRSS" fileSize="139179224" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/5b234f6039a37e2b455e337b8421aa5688a098e4_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/5b234f6039a37e2b455e337b8421aa5688a098e4_2880x1620.jpg?"/>
    </item>
    <item>
      <title>What you can do to prevent Alzheimer's | Lisa Genova</title>
      <itunes:author>Lisa Genova</itunes:author>
      <description>
        <![CDATA[Alzheimer's doesn't have to be your brain's destiny, says neuroscientist and author of "Still Alice," Lisa Genova. She shares the latest science investigating the disease -- and some promising research on what each of us can do to build an Alzheimer's-resistant brain.]]>
      </description>
      <itunes:subtitle>What you can do to prevent Alzheimer's | Lisa Genova</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Alzheimer's doesn't have to be your brain's destiny, says neuroscientist and author of "Still Alice," Lisa Genova. She shares the latest science investigating the disease -- and some promising research on what each of us can do to build an Alzheimer's-resistant brain.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/LisaGenova_2017.mp4?apikey=TEDRSS" length="47814531"/>
      <link>https://www.ted.com/talks/lisa_genova_what_you_can_do_to_prevent_alzheimer_s?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2771</guid>
      <jwplayer:talkId>2771</jwplayer:talkId>
      <pubDate>Fri, 28 Apr 2017 17:07:37 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:13:56</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/LisaGenova_2017.mp4?apikey=TEDRSS" fileSize="47814531" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/cdb073dfa8128eb670ecce1122c050ef95c59ff4_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/cdb073dfa8128eb670ecce1122c050ef95c59ff4_2880x1620.jpg?"/>
    </item>
    <item>
      <title>On tennis, love and motherhood | Serena Williams and Gayle King</title>
      <itunes:author>Serena Williams and Gayle King</itunes:author>
      <description>
        <![CDATA[Twenty-three Grand Slam titles later, tennis superstar Serena Williams sits down with journalist Gayle King to share a warm, mischievous conversation about her life, love, wins and losses -- starting with the story of how she accidentally shared her pregnancy news with the world.]]>
      </description>
      <itunes:subtitle>On tennis, love and motherhood | Serena Williams and Gayle King</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Twenty-three Grand Slam titles later, tennis superstar Serena Williams sits down with journalist Gayle King to share a warm, mischievous conversation about her life, love, wins and losses -- starting with the story of how she accidentally shared her pregnancy news with the world.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/SerenaWilliams_2017.mp4?apikey=TEDRSS" length="63179556"/>
      <link>https://www.ted.com/talks/serena_williams_gayle_king_on_tennis_love_and_motherhood?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2770</guid>
      <jwplayer:talkId>2770</jwplayer:talkId>
      <pubDate>Thu, 27 Apr 2017 18:50:06 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:18:28</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/SerenaWilliams_2017.mp4?apikey=TEDRSS" fileSize="63179556" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/ea594390cecc864357ebd75866ab69a68ea141f9_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/ea594390cecc864357ebd75866ab69a68ea141f9_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Why the only future worth building includes everyone | His Holiness Pope Francis</title>
      <itunes:author>His Holiness Pope Francis</itunes:author>
      <description>
        <![CDATA[A single individual is enough for hope to exist, and that individual can be you, says His Holiness Pope Francis in this searing TED Talk delivered directly from Vatican City. In a hopeful message to people of all faiths, to those who have power as well as those who don't, the spiritual leader provides illuminating commentary on the world as we currently find it and calls for equality, solidarity and tenderness to prevail. "Let us help each other, all together, to remember that the 'other' is not a statistic, or a number," he says. "We all need each other."]]>
      </description>
      <itunes:subtitle>Why the only future worth building includes everyone | His Holiness Pope Francis</itunes:subtitle>
      <itunes:summary>
        <![CDATA[A single individual is enough for hope to exist, and that individual can be you, says His Holiness Pope Francis in this searing TED Talk delivered directly from Vatican City. In a hopeful message to people of all faiths, to those who have power as well as those who don't, the spiritual leader provides illuminating commentary on the world as we currently find it and calls for equality, solidarity and tenderness to prevail. "Let us help each other, all together, to remember that the 'other' is not a statistic, or a number," he says. "We all need each other."]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/PopeFrancis_2017-64k.mp4?apikey=TEDRSS" length="8817938"/>
      <link>https://www.ted.com/talks/pope_francis_why_the_only_future_worth_building_includes_everyone?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2744</guid>
      <jwplayer:talkId>2744</jwplayer:talkId>
      <pubDate>Wed, 26 Apr 2017 01:01:05 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:17:52</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/PopeFrancis_2017-64k.mp4?apikey=TEDRSS" fileSize="8817938" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/ce1b24b3c799d95b5dec38dbd927102c03b016d3_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/ce1b24b3c799d95b5dec38dbd927102c03b016d3_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Science in service to the public good | Siddhartha Roy</title>
      <itunes:author>Siddhartha Roy</itunes:author>
      <description>
        <![CDATA[We give scientists and engineers great technical training, but we're not as good at teaching ethical decision-making or building character. Take, for example, the environmental crisis that recently unfolded in Flint, Michigan -- and the professionals there who did nothing to fix it. Siddhartha Roy helped prove that Flint's water was contaminated, and he tells a story of science in service to the public good, calling on the next generation of scientists and engineers to dedicate their work to protecting people and the planet.]]>
      </description>
      <itunes:subtitle>Science in service to the public good | Siddhartha Roy</itunes:subtitle>
      <itunes:summary>
        <![CDATA[We give scientists and engineers great technical training, but we're not as good at teaching ethical decision-making or building character. Take, for example, the environmental crisis that recently unfolded in Flint, Michigan -- and the professionals there who did nothing to fix it. Siddhartha Roy helped prove that Flint's water was contaminated, and he tells a story of science in service to the public good, calling on the next generation of scientists and engineers to dedicate their work to protecting people and the planet.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/SiddharthaRoy_2016X.mp4?apikey=TEDRSS" length="50122373"/>
      <link>https://www.ted.com/talks/siddhartha_roy_science_in_service_to_the_public_good?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2738</guid>
      <jwplayer:talkId>2738</jwplayer:talkId>
      <pubDate>Tue, 25 Apr 2017 15:22:02 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:14:33</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/SiddharthaRoy_2016X.mp4?apikey=TEDRSS" fileSize="50122373" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/817dc0649790312f28cc4aa5c14c0cc35473c22a_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/817dc0649790312f28cc4aa5c14c0cc35473c22a_2880x1620.jpg?"/>
    </item>
    <item>
      <title>How fake news does real harm | Stephanie Busari</title>
      <itunes:author>Stephanie Busari</itunes:author>
      <description>
        <![CDATA[On April 14, 2014, the terrorist organization Boko Haram kidnapped more than 200 schoolgirls from the town of Chibok, Nigeria. Around the world, the crime became epitomized by the slogan #BringBackOurGirls -- but in Nigeria, government officials called the crime a hoax, confusing and delaying efforts to rescue the girls. In this powerful talk, journalist Stephanie Busari points to the Chibok tragedy to explain the deadly danger of fake news and what we can do to stop it.]]>
      </description>
      <itunes:subtitle>How fake news does real harm | Stephanie Busari</itunes:subtitle>
      <itunes:summary>
        <![CDATA[On April 14, 2014, the terrorist organization Boko Haram kidnapped more than 200 schoolgirls from the town of Chibok, Nigeria. Around the world, the crime became epitomized by the slogan #BringBackOurGirls -- but in Nigeria, government officials called the crime a hoax, confusing and delaying efforts to rescue the girls. In this powerful talk, journalist Stephanie Busari points to the Chibok tragedy to explain the deadly danger of fake news and what we can do to stop it.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/StephanieBusari_2017S.mp4?apikey=TEDRSS" length="22370446"/>
      <link>https://www.ted.com/talks/stephanie_busari_how_fake_news_does_real_harm?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2737</guid>
      <jwplayer:talkId>2737</jwplayer:talkId>
      <pubDate>Mon, 24 Apr 2017 15:12:07 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:06:26</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/StephanieBusari_2017S.mp4?apikey=TEDRSS" fileSize="22370446" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/1eaa063a9f0cadc20d2bd4bdca54920d0b2bf46e_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/1eaa063a9f0cadc20d2bd4bdca54920d0b2bf46e_2880x1620.jpg?"/>
    </item>
    <item>
      <title>How I learned to read -- and trade stocks -- in prison | Curtis "Wall Street" Carroll</title>
      <itunes:author>Curtis "Wall Street" Carroll</itunes:author>
      <description>
        <![CDATA[Financial literacy isn't a skill -- it's a lifestyle. Take it from Curtis "Wall Street" Carroll. As an incarcerated individual, Carroll knows the power of a dollar. While in prison, he taught himself how to read and trade stocks, and now he shares a simple, powerful message: we all need to be more savvy with our money.]]>
      </description>
      <itunes:subtitle>How I learned to read -- and trade stocks -- in prison | Curtis "Wall Street" Carroll</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Financial literacy isn't a skill -- it's a lifestyle. Take it from Curtis "Wall Street" Carroll. As an incarcerated individual, Carroll knows the power of a dollar. While in prison, he taught himself how to read and trade stocks, and now he shares a simple, powerful message: we all need to be more savvy with our money.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/CurtisWallStreetCarroll_2016X.mp4?apikey=TEDRSS" length="38090674"/>
      <link>https://www.ted.com/talks/curtis_wall_street_carroll_how_i_learned_to_read_and_trade_stocks_in_prison?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2741</guid>
      <jwplayer:talkId>2741</jwplayer:talkId>
      <pubDate>Fri, 21 Apr 2017 15:00:21 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:11:03</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/CurtisWallStreetCarroll_2016X.mp4?apikey=TEDRSS" fileSize="38090674" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/133c6fea04fd9fc79f9745a55abedd536822827a_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/133c6fea04fd9fc79f9745a55abedd536822827a_2880x1620.jpg?"/>
    </item>
    <item>
      <title>A doctor's case for medical marijuana | David Casarett</title>
      <itunes:author>David Casarett</itunes:author>
      <description>
        <![CDATA[Physician David Casarett was tired of hearing hype and half-truths around medical marijuana, so he put on his skeptic's hat and investigated on his own. He comes back with a fascinating report on what we know and what we don't -- and what mainstream medicine could learn from the modern medical marijuana dispensary.]]>
      </description>
      <itunes:subtitle>A doctor's case for medical marijuana | David Casarett</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Physician David Casarett was tired of hearing hype and half-truths around medical marijuana, so he put on his skeptic's hat and investigated on his own. He comes back with a fascinating report on what we know and what we don't -- and what mainstream medicine could learn from the modern medical marijuana dispensary.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/DavidCasarett_2016P.mp4?apikey=TEDRSS" length="51767536"/>
      <link>https://www.ted.com/talks/david_casarett_a_doctor_s_case_for_medical_marijuana?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2735</guid>
      <jwplayer:talkId>2735</jwplayer:talkId>
      <pubDate>Thu, 20 Apr 2017 15:04:30 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:15:07</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/DavidCasarett_2016P.mp4?apikey=TEDRSS" fileSize="51767536" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/71ecfcc687ee20218afce92ea0dc23c6456e35e0_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/71ecfcc687ee20218afce92ea0dc23c6456e35e0_2880x1620.jpg?"/>
    </item>
    <item>
      <title>A video game to cope with grief | Amy Green</title>
      <itunes:author>Amy Green</itunes:author>
      <description>
        <![CDATA[When Amy Green's young son was diagnosed with a rare brain tumor, she made up a bedtime story for his siblings to teach them about cancer. What resulted was a video game, "That Dragon, Cancer," which takes players on a journey they can't win. In this beautiful talk about coping with loss, Green brings joy and play to tragedy. "We made a game that's hard to play," she says, "because the hardest moments of our lives change us more than any goal we could ever accomplish."]]>
      </description>
      <itunes:subtitle>A video game to cope with grief | Amy Green</itunes:subtitle>
      <itunes:summary>
        <![CDATA[When Amy Green's young son was diagnosed with a rare brain tumor, she made up a bedtime story for his siblings to teach them about cancer. What resulted was a video game, "That Dragon, Cancer," which takes players on a journey they can't win. In this beautiful talk about coping with loss, Green brings joy and play to tragedy. "We made a game that's hard to play," she says, "because the hardest moments of our lives change us more than any goal we could ever accomplish."]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/AmyGreen_2017S.mp4?apikey=TEDRSS" length="36552851"/>
      <link>https://www.ted.com/talks/amy_green_a_video_game_to_cope_with_grief?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2733</guid>
      <jwplayer:talkId>2733</jwplayer:talkId>
      <pubDate>Wed, 19 Apr 2017 14:59:14 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:10:34</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/AmyGreen_2017S.mp4?apikey=TEDRSS" fileSize="36552851" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/413172e9cfce0efa8bfb3f7730853ea61c4f969d_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/413172e9cfce0efa8bfb3f7730853ea61c4f969d_2880x1620.jpg?"/>
    </item>
    <item>
      <title>How radio telescopes show us unseen galaxies | Natasha Hurley-Walker</title>
      <itunes:author>Natasha Hurley-Walker</itunes:author>
      <description>
        <![CDATA[Our universe is strange, wonderful and vast, says astronomer Natasha Hurley-Walker. A spaceship can't carry you into its depths (yet) -- but a radio telescope can. In this mesmerizing talk, Hurley-Walker shows how she probes the mysteries of the universe using special technology that reveals light spectrums we can't see.]]>
      </description>
      <itunes:subtitle>How radio telescopes show us unseen galaxies | Natasha Hurley-Walker</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Our universe is strange, wonderful and vast, says astronomer Natasha Hurley-Walker. A spaceship can't carry you into its depths (yet) -- but a radio telescope can. In this mesmerizing talk, Hurley-Walker shows how she probes the mysteries of the universe using special technology that reveals light spectrums we can't see.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/NatashaHurleyWalker_2016X.mp4?apikey=TEDRSS" length="53055533"/>
      <link>https://www.ted.com/talks/natasha_hurley_walker_how_radio_telescopes_show_us_unseen_galaxies?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2736</guid>
      <jwplayer:talkId>2736</jwplayer:talkId>
      <pubDate>Tue, 18 Apr 2017 15:02:39 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:15:25</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/NatashaHurleyWalker_2016X.mp4?apikey=TEDRSS" fileSize="53055533" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/4d92d229412791ad69ddb89fc52aea0079aed8d6_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/4d92d229412791ad69ddb89fc52aea0079aed8d6_2880x1620.jpg?"/>
    </item>
    <item>
      <title>How do you build a sacred space? | Siamak Hariri</title>
      <itunes:author>Siamak Hariri</itunes:author>
      <description>
        <![CDATA[To design the Bahá'í Temple of South America, architect Siamak Hariri focused on illumination -- from the temple's form, which captures the movement of the sun throughout the day, to the iridescent, luminous stone and glass used to construct it. Join Hariri for a journey through the creative process, as he explores what makes for a sacred experience in a secular world.]]>
      </description>
      <itunes:subtitle>How do you build a sacred space? | Siamak Hariri</itunes:subtitle>
      <itunes:summary>
        <![CDATA[To design the Bahá'í Temple of South America, architect Siamak Hariri focused on illumination -- from the temple's form, which captures the movement of the sun throughout the day, to the iridescent, luminous stone and glass used to construct it. Join Hariri for a journey through the creative process, as he explores what makes for a sacred experience in a secular world.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/SiamakHariri_2017S.mp4?apikey=TEDRSS" length="43998597"/>
      <link>https://www.ted.com/talks/siamak_hariri_how_do_you_build_a_sacred_space?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2734</guid>
      <jwplayer:talkId>2734</jwplayer:talkId>
      <pubDate>Mon, 17 Apr 2017 14:48:33 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:12:46</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/SiamakHariri_2017S.mp4?apikey=TEDRSS" fileSize="43998597" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/2c9af057b4ca96f5d625d3110d5439b60fc22c5d_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/2c9af057b4ca96f5d625d3110d5439b60fc22c5d_2880x1620.jpg?"/>
    </item>
    <item>
      <title>We should all be feminists | Chimamanda Ngozi Adichie</title>
      <itunes:author>Chimamanda Ngozi Adichie</itunes:author>
      <description>
        <![CDATA[We teach girls that they can have ambition, but not too much ... to be successful, but not too successful, or they'll threaten men, says author Chimamanda Ngozi Adichie. In this classic talk that started a worldwide conversation about feminism, Adichie asks that we begin to dream about and plan for a different, fairer world -- of happier men and women who are truer to themselves.]]>
      </description>
      <itunes:subtitle>We should all be feminists | Chimamanda Ngozi Adichie</itunes:subtitle>
      <itunes:summary>
        <![CDATA[We teach girls that they can have ambition, but not too much ... to be successful, but not too successful, or they'll threaten men, says author Chimamanda Ngozi Adichie. In this classic talk that started a worldwide conversation about feminism, Adichie asks that we begin to dream about and plan for a different, fairer world -- of happier men and women who are truer to themselves.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/ChimamandaNgoziAdichie_2012X.mp4?apikey=TEDRSS" length="100668146"/>
      <link>https://www.ted.com/talks/chimamanda_ngozi_adichie_we_should_all_be_feminists?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2732</guid>
      <jwplayer:talkId>2732</jwplayer:talkId>
      <pubDate>Fri, 14 Apr 2017 14:52:57 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:29:28</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/ChimamandaNgoziAdichie_2012X.mp4?apikey=TEDRSS" fileSize="100668146" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/2bc5b10c49af2ba3417238e66da50972f2bb3d17_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/2bc5b10c49af2ba3417238e66da50972f2bb3d17_2880x1620.jpg?"/>
    </item>
    <item>
      <title>A simple birth kit for mothers in the developing world | Zubaida Bai</title>
      <itunes:author>Zubaida Bai</itunes:author>
      <description>
        <![CDATA[TED Fellow Zubaida Bai works with medical professionals, midwives and mothers to bring dignity and low-cost interventions to women's health care. In this quick, inspiring talk, she presents her clean birth kit in a purse, which contains everything a new mother needs for a hygienic birth and a healthy delivery -- no matter where in the world (or how far from a medical clinic) she might be.]]>
      </description>
      <itunes:subtitle>A simple birth kit for mothers in the developing world | Zubaida Bai</itunes:subtitle>
      <itunes:summary>
        <![CDATA[TED Fellow Zubaida Bai works with medical professionals, midwives and mothers to bring dignity and low-cost interventions to women's health care. In this quick, inspiring talk, she presents her clean birth kit in a purse, which contains everything a new mother needs for a hygienic birth and a healthy delivery -- no matter where in the world (or how far from a medical clinic) she might be.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/ZubaidaBai_2016S.mp4?apikey=TEDRSS" length="23406773"/>
      <link>https://www.ted.com/talks/zubaida_bai_a_simple_birth_kit_for_mothers_in_the_developing_world?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2731</guid>
      <jwplayer:talkId>2731</jwplayer:talkId>
      <pubDate>Thu, 13 Apr 2017 15:06:11 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:06:44</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/ZubaidaBai_2016S.mp4?apikey=TEDRSS" fileSize="23406773" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/e9b5b4e3cf862978bdcccb0aae17c0de1aa9e830_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/e9b5b4e3cf862978bdcccb0aae17c0de1aa9e830_2880x1620.jpg?"/>
    </item>
    <item>
      <title>An intergalactic guide to using a defibrillator | Todd Scott</title>
      <itunes:author>Todd Scott</itunes:author>
      <description>
        <![CDATA[If Yoda goes into cardiac arrest, will you know what to do? Artist and first-aid enthusiast Todd Scott breaks down what you need to know about using an automated external defibrillator, or AED -- in this galaxy and ones that are far, far away. Prepare to save the life of a Jedi, Chewbacca (he'll need a quick shave first) or someone else in need with some helpful pointers.]]>
      </description>
      <itunes:subtitle>An intergalactic guide to using a defibrillator | Todd Scott</itunes:subtitle>
      <itunes:summary>
        <![CDATA[If Yoda goes into cardiac arrest, will you know what to do? Artist and first-aid enthusiast Todd Scott breaks down what you need to know about using an automated external defibrillator, or AED -- in this galaxy and ones that are far, far away. Prepare to save the life of a Jedi, Chewbacca (he'll need a quick shave first) or someone else in need with some helpful pointers.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/ToddScott_2017S.mp4?apikey=TEDRSS" length="19019956"/>
      <link>https://www.ted.com/talks/todd_scott_an_intergalactic_guide_to_using_a_defibrillator?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2729</guid>
      <jwplayer:talkId>2729</jwplayer:talkId>
      <pubDate>Wed, 12 Apr 2017 14:56:24 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:05:22</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/ToddScott_2017S.mp4?apikey=TEDRSS" fileSize="19019956" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/35283a1a5bff146ac634df6dcd7a1cba30ca11de_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/35283a1a5bff146ac634df6dcd7a1cba30ca11de_2880x1620.jpg?"/>
    </item>
    <item>
      <title>In praise of conflict | Jonathan Marks</title>
      <itunes:author>Jonathan Marks</itunes:author>
      <description>
        <![CDATA[Conflict is bad; compromise, consensus and collaboration are good -- or so we're told. Lawyer and bioethicist Jonathan Marks challenges this conventional wisdom, showing how governments can jeopardize public health, human rights and the environment when they partner with industry. An important, timely reminder that common good and common ground are not the same thing.]]>
      </description>
      <itunes:subtitle>In praise of conflict | Jonathan Marks</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Conflict is bad; compromise, consensus and collaboration are good -- or so we're told. Lawyer and bioethicist Jonathan Marks challenges this conventional wisdom, showing how governments can jeopardize public health, human rights and the environment when they partner with industry. An important, timely reminder that common good and common ground are not the same thing.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/JonathanMarks_2017X.mp4?apikey=TEDRSS" length="51347734"/>
      <link>https://www.ted.com/talks/jonathan_marks_in_praise_of_conflict?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2730</guid>
      <jwplayer:talkId>2730</jwplayer:talkId>
      <pubDate>Tue, 11 Apr 2017 15:07:12 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:14:56</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/JonathanMarks_2017X.mp4?apikey=TEDRSS" fileSize="51347734" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/252d75e3eb7901fdd39c6fc427c0268cfed0a996_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/252d75e3eb7901fdd39c6fc427c0268cfed0a996_2880x1620.jpg?"/>
    </item>
    <item>
      <title>3 ways to plan for the (very) long term | Ari Wallach</title>
      <itunes:author>Ari Wallach</itunes:author>
      <description>
        <![CDATA[We increasingly make decisions based on short-term goals and gains -- an approach that makes the future more uncertain and less safe. How can we learn to think about and plan for a better future in the long term ... like, grandchildren-scale long term? Ari Wallach shares three tactics for thinking beyond the immediate.]]>
      </description>
      <itunes:subtitle>3 ways to plan for the (very) long term | Ari Wallach</itunes:subtitle>
      <itunes:summary>
        <![CDATA[We increasingly make decisions based on short-term goals and gains -- an approach that makes the future more uncertain and less safe. How can we learn to think about and plan for a better future in the long term ... like, grandchildren-scale long term? Ari Wallach shares three tactics for thinking beyond the immediate.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/AriWallach_2016X.mp4?apikey=TEDRSS" length="47208980"/>
      <link>https://www.ted.com/talks/ari_wallach_3_ways_to_plan_for_the_very_long_term?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2728</guid>
      <jwplayer:talkId>2728</jwplayer:talkId>
      <pubDate>Mon, 10 Apr 2017 14:58:16 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:13:42</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/AriWallach_2016X.mp4?apikey=TEDRSS" fileSize="47208980" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/cb2a82558d31158734d32afe88cd25226332a966_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/cb2a82558d31158734d32afe88cd25226332a966_2880x1620.jpg?"/>
    </item>
    <item>
      <title>How we can find ourselves in data | Giorgia Lupi</title>
      <itunes:author>Giorgia Lupi</itunes:author>
      <description>
        <![CDATA[Giorgia Lupi uses data to tell human stories, adding nuance to numbers. In this charming talk, she shares how we can bring personality to data, visualizing even the mundane details of our daily lives and transforming the abstract and uncountable into something that can be seen, felt and directly reconnected to our lives.]]>
      </description>
      <itunes:subtitle>How we can find ourselves in data | Giorgia Lupi</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Giorgia Lupi uses data to tell human stories, adding nuance to numbers. In this charming talk, she shares how we can bring personality to data, visualizing even the mundane details of our daily lives and transforming the abstract and uncountable into something that can be seen, felt and directly reconnected to our lives.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/GiorgiaLupi_2017S.mp4?apikey=TEDRSS" length="38825883"/>
      <link>https://www.ted.com/talks/giorgia_lupi_how_we_can_find_ourselves_in_data?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2727</guid>
      <jwplayer:talkId>2727</jwplayer:talkId>
      <pubDate>Fri, 07 Apr 2017 15:07:40 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:11:13</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/GiorgiaLupi_2017S.mp4?apikey=TEDRSS" fileSize="38825883" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/9ca3922cace721320cd2b5de1f5761f7a75b2010_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/9ca3922cace721320cd2b5de1f5761f7a75b2010_2880x1620.jpg?"/>
    </item>
    <item>
      <title>How racism makes us sick | David R. Williams</title>
      <itunes:author>David R. Williams</itunes:author>
      <description>
        <![CDATA[Why does race matter so profoundly for health? David R. Williams developed a scale to measure the impact of discrimination on well-being, going beyond traditional measures like income and education to reveal how factors like implicit bias, residential segregation and negative stereotypes create and sustain inequality. In this eye-opening talk, Williams presents evidence for how racism is producing a rigged system -- and offers hopeful examples of programs across the US that are working to dismantle discrimination.]]>
      </description>
      <itunes:subtitle>How racism makes us sick | David R. Williams</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Why does race matter so profoundly for health? David R. Williams developed a scale to measure the impact of discrimination on well-being, going beyond traditional measures like income and education to reveal how factors like implicit bias, residential segregation and negative stereotypes create and sustain inequality. In this eye-opening talk, Williams presents evidence for how racism is producing a rigged system -- and offers hopeful examples of programs across the US that are working to dismantle discrimination.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/DavidRWilliams_2016P.mp4?apikey=TEDRSS" length="59752326"/>
      <link>https://www.ted.com/talks/david_r_williams_how_racism_makes_us_sick?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2726</guid>
      <jwplayer:talkId>2726</jwplayer:talkId>
      <pubDate>Thu, 06 Apr 2017 15:03:43 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:17:27</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/DavidRWilliams_2016P.mp4?apikey=TEDRSS" fileSize="59752326" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/d662b07a1534e5aeef995fc4d97a561a1f21a52b_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/d662b07a1534e5aeef995fc4d97a561a1f21a52b_2880x1620.jpg?"/>
    </item>
    <item>
      <title>The conversation we're not having about digital child abuse | Sebastián Bortnik</title>
      <itunes:author>Sebastián Bortnik</itunes:author>
      <description>
        <![CDATA[We need to talk to kids about the risks they face online, says information security expert Sebastián Bortnik. In this talk, Bortnik discusses the issue of "grooming" -- the sexual predation of children by adults on the internet -- and outlines the conversations we need to start having about technology to keep our kids safe. (In Spanish with English subtitles)]]>
      </description>
      <itunes:subtitle>The conversation we're not having about digital child abuse | Sebastián Bortnik</itunes:subtitle>
      <itunes:summary>
        <![CDATA[We need to talk to kids about the risks they face online, says information security expert Sebastián Bortnik. In this talk, Bortnik discusses the issue of "grooming" -- the sexual predation of children by adults on the internet -- and outlines the conversations we need to start having about technology to keep our kids safe. (In Spanish with English subtitles)]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/SebastianBortnik_2016X.mp4?apikey=TEDRSS" length="47400042"/>
      <link>https://www.ted.com/talks/sebastian_bortnik_the_conversation_we_re_not_having_about_digital_child_abuse?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2725</guid>
      <jwplayer:talkId>2725</jwplayer:talkId>
      <pubDate>Wed, 05 Apr 2017 14:57:47 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:13:45</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/SebastianBortnik_2016X.mp4?apikey=TEDRSS" fileSize="47400042" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/8e688b38b7f010c53dd2c31b9b309032fc5cf7c4_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/8e688b38b7f010c53dd2c31b9b309032fc5cf7c4_2880x1620.jpg?"/>
    </item>
    <item>
      <title>How to take a picture of a black hole | Katie Bouman</title>
      <itunes:author>Katie Bouman</itunes:author>
      <description>
        <![CDATA[At the heart of the Milky Way, there's a supermassive black hole that feeds off a spinning disk of hot gas, sucking up anything that ventures too close -- even light. We can't see it, but its event horizon casts a shadow, and an image of that shadow could help answer some important questions about the universe. Scientists used to think that making such an image would require a telescope the size of Earth -- until Katie Bouman and a team of astronomers came up with a clever alternative. Bouman explains how we can take a picture of the ultimate dark using the Event Horizon Telescope.]]>
      </description>
      <itunes:subtitle>How to take a picture of a black hole | Katie Bouman</itunes:subtitle>
      <itunes:summary>
        <![CDATA[At the heart of the Milky Way, there's a supermassive black hole that feeds off a spinning disk of hot gas, sucking up anything that ventures too close -- even light. We can't see it, but its event horizon casts a shadow, and an image of that shadow could help answer some important questions about the universe. Scientists used to think that making such an image would require a telescope the size of Earth -- until Katie Bouman and a team of astronomers came up with a clever alternative. Bouman explains how we can take a picture of the ultimate dark using the Event Horizon Telescope.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/KatieBouman_2016X.mp4?apikey=TEDRSS" length="44393473"/>
      <link>https://www.ted.com/talks/katie_bouman_what_does_a_black_hole_look_like?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2723</guid>
      <jwplayer:talkId>2723</jwplayer:talkId>
      <pubDate>Tue, 04 Apr 2017 15:10:25 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:12:51</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/KatieBouman_2016X.mp4?apikey=TEDRSS" fileSize="44393473" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/bb720ff9ead8d6ca764da9aff093bf27b819a72e_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/bb720ff9ead8d6ca764da9aff093bf27b819a72e_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Political common ground in a polarized United States | Gretchen Carlson, David Brooks</title>
      <itunes:author>Gretchen Carlson, David Brooks</itunes:author>
      <description>
        <![CDATA[How can we bridge the gap between left and right to have a wiser, more connected political conversation? Journalist Gretchen Carlson and op-ed columnist David Brooks share insights on the tensions at the heart of American politics today -- and where we can find common ground. Followed by a rousing performance of "America the Beautiful" by Vy Higginsen's Gospel Choir of Harlem.]]>
      </description>
      <itunes:subtitle>Political common ground in a polarized United States | Gretchen Carlson, David Brooks</itunes:subtitle>
      <itunes:summary>
        <![CDATA[How can we bridge the gap between left and right to have a wiser, more connected political conversation? Journalist Gretchen Carlson and op-ed columnist David Brooks share insights on the tensions at the heart of American politics today -- and where we can find common ground. Followed by a rousing performance of "America the Beautiful" by Vy Higginsen's Gospel Choir of Harlem.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/GretchenCarlsonandDavidBrooks_2017S.mp4?apikey=TEDRSS" length="162216277"/>
      <link>https://www.ted.com/talks/gretchen_carlson_david_brooks_political_common_ground_in_a_polarized_united_states?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2716</guid>
      <jwplayer:talkId>2716</jwplayer:talkId>
      <pubDate>Mon, 03 Apr 2017 21:50:55 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:47:33</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/GretchenCarlsonandDavidBrooks_2017S.mp4?apikey=TEDRSS" fileSize="162216277" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/7c3e47242138e07fca8739cf8c88c0dd25f3ad86_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/7c3e47242138e07fca8739cf8c88c0dd25f3ad86_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Know your worth, and then ask for it | Casey Brown</title>
      <itunes:author>Casey Brown</itunes:author>
      <description>
        <![CDATA[Your boss probably isn't paying you what you're worth -- instead, they're paying you what they think you're worth. Take the time to learn how to shape their thinking. Pricing consultant Casey Brown shares helpful stories and learnings that can help you better communicate your value and get paid for your excellence.]]>
      </description>
      <itunes:subtitle>Know your worth, and then ask for it | Casey Brown</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Your boss probably isn't paying you what you're worth -- instead, they're paying you what they think you're worth. Take the time to learn how to shape their thinking. Pricing consultant Casey Brown shares helpful stories and learnings that can help you better communicate your value and get paid for your excellence.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/CaseyBrown_2015X.mp4?apikey=TEDRSS" length="29094917"/>
      <link>https://www.ted.com/talks/casey_brown_know_your_worth_and_then_ask_for_it?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2724</guid>
      <jwplayer:talkId>2724</jwplayer:talkId>
      <pubDate>Mon, 03 Apr 2017 14:51:26 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:08:22</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/CaseyBrown_2015X.mp4?apikey=TEDRSS" fileSize="29094917" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/f69a6e378c328b9d61ae131923a410e887dd34a4_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/f69a6e378c328b9d61ae131923a410e887dd34a4_2880x1620.jpg?"/>
    </item>
    <item>
      <title>A young poet tells the story of Darfur | Emtithal Mahmoud</title>
      <itunes:author>Emtithal Mahmoud</itunes:author>
      <description>
        <![CDATA[Emtithal "Emi" Mahmoud writes poetry of resilience, confronting her experience of escaping the genocide in Darfur in verse. She shares two stirring original poems about refugees, family, joy and sorrow, asking, "Will you witness me?"]]>
      </description>
      <itunes:subtitle>A young poet tells the story of Darfur | Emtithal Mahmoud</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Emtithal "Emi" Mahmoud writes poetry of resilience, confronting her experience of escaping the genocide in Darfur in verse. She shares two stirring original poems about refugees, family, joy and sorrow, asking, "Will you witness me?"]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/EmtithalMahmoud_2016P.mp4?apikey=TEDRSS" length="37316292"/>
      <link>https://www.ted.com/talks/emtithal_mahmoud_a_young_poet_tells_the_story_of_darfur?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2722</guid>
      <jwplayer:talkId>2722</jwplayer:talkId>
      <pubDate>Fri, 31 Mar 2017 15:12:16 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:10:51</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/EmtithalMahmoud_2016P.mp4?apikey=TEDRSS" fileSize="37316292" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/156513c31ca187a831e783084a29f3a4c6949570_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/156513c31ca187a831e783084a29f3a4c6949570_2880x1620.jpg?"/>
    </item>
    <item>
      <title>"Music for Wood and Strings" | Sō Percussion</title>
      <itunes:author>Sō Percussion</itunes:author>
      <description>
        <![CDATA[Sō Percussion creates adventurous compositions with new, unconventional instruments. Performing "Music for Wood and Strings" by Bryce Dessner of The National, the quartet plays custom-made dulcimer-like instruments that combine the sound of an electric guitar with the percussionist's toolkit to create a hypnotic effect.]]>
      </description>
      <itunes:subtitle>"Music for Wood and Strings" | Sō Percussion</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Sō Percussion creates adventurous compositions with new, unconventional instruments. Performing "Music for Wood and Strings" by Bryce Dessner of The National, the quartet plays custom-made dulcimer-like instruments that combine the sound of an electric guitar with the percussionist's toolkit to create a hypnotic effect.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/SoPercussion_2016.mp4?apikey=TEDRSS" length="34884992"/>
      <link>https://www.ted.com/talks/so_percussion_music_for_wood_and_strings?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2717</guid>
      <jwplayer:talkId>2717</jwplayer:talkId>
      <pubDate>Fri, 31 Mar 2017 12:34:06 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:10:09</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/SoPercussion_2016.mp4?apikey=TEDRSS" fileSize="34884992" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/9100e09f31c99a9b7c10bd9336e4ecc195bfee8b_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/9100e09f31c99a9b7c10bd9336e4ecc195bfee8b_2880x1620.jpg?"/>
    </item>
    <item>
      <title>How early life experience is written into DNA | Moshe Szyf</title>
      <itunes:author>Moshe Szyf</itunes:author>
      <description>
        <![CDATA[Moshe Szyf is a pioneer in the field of epigenetics, the study of how living things reprogram their genome in response to social factors like stress and lack of food. His research suggests that biochemical signals passed from mothers to offspring tell the child what kind of world they're going to live in, changing the expression of genes. "DNA isn't just a sequence of letters; it's not just a script." Szyf says. "DNA is a dynamic movie in which our experiences are being written."]]>
      </description>
      <itunes:subtitle>How early life experience is written into DNA | Moshe Szyf</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Moshe Szyf is a pioneer in the field of epigenetics, the study of how living things reprogram their genome in response to social factors like stress and lack of food. His research suggests that biochemical signals passed from mothers to offspring tell the child what kind of world they're going to live in, changing the expression of genes. "DNA isn't just a sequence of letters; it's not just a script." Szyf says. "DNA is a dynamic movie in which our experiences are being written."]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/MosheSzyf_2016X.mp4?apikey=TEDRSS" length="56938843"/>
      <link>https://www.ted.com/talks/moshe_szyf_how_early_life_experience_is_written_into_dna?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2721</guid>
      <jwplayer:talkId>2721</jwplayer:talkId>
      <pubDate>Thu, 30 Mar 2017 15:17:00 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:16:35</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/MosheSzyf_2016X.mp4?apikey=TEDRSS" fileSize="56938843" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/b09fe085ec5fc693df468ca6be98c969425c2015_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/b09fe085ec5fc693df468ca6be98c969425c2015_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Addiction is a disease. We should treat it like one | Michael Botticelli</title>
      <itunes:author>Michael Botticelli</itunes:author>
      <description>
        <![CDATA[Only one in nine people in the United States gets the care and treatment they need for addiction and substance abuse. A former Director of National Drug Control Policy, Michael Botticelli is working to end this epidemic and treat people with addictions with kindness, compassion and fairness. In a personal, thoughtful talk, he encourages the millions of Americans in recovery today to make their voices heard and confront the stigma associated with substance use disorders.]]>
      </description>
      <itunes:subtitle>Addiction is a disease. We should treat it like one | Michael Botticelli</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Only one in nine people in the United States gets the care and treatment they need for addiction and substance abuse. A former Director of National Drug Control Policy, Michael Botticelli is working to end this epidemic and treat people with addictions with kindness, compassion and fairness. In a personal, thoughtful talk, he encourages the millions of Americans in recovery today to make their voices heard and confront the stigma associated with substance use disorders.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/MichaelBotticelli_2016X.mp4?apikey=TEDRSS" length="37052983"/>
      <link>https://www.ted.com/talks/michael_botticelli_addiction_is_a_disease_we_should_treat_it_like_one?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2693</guid>
      <jwplayer:talkId>2693</jwplayer:talkId>
      <pubDate>Wed, 29 Mar 2017 15:01:11 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:10:44</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/MichaelBotticelli_2016X.mp4?apikey=TEDRSS" fileSize="37052983" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/564befd037b0c17d0c3ee4b11c5cc2b8d5d8313c_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/564befd037b0c17d0c3ee4b11c5cc2b8d5d8313c_2880x1620.jpg?"/>
    </item>
    <item>
      <title>What we don't know about mother's milk | Katie Hinde</title>
      <itunes:author>Katie Hinde</itunes:author>
      <description>
        <![CDATA[Breast milk grows babies' bodies, fuels neurodevelopment, provides essential immunofactors and safeguards against famine and disease -- why, then, does science know more about tomatoes than mother's milk? Katie Hinde shares insights into this complex, life-giving substance and discusses the major gaps scientific research still needs to fill so we can better understand it.]]>
      </description>
      <itunes:subtitle>What we don't know about mother's milk | Katie Hinde</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Breast milk grows babies' bodies, fuels neurodevelopment, provides essential immunofactors and safeguards against famine and disease -- why, then, does science know more about tomatoes than mother's milk? Katie Hinde shares insights into this complex, life-giving substance and discusses the major gaps scientific research still needs to fill so we can better understand it.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/KatieHinde_2016W.mp4?apikey=TEDRSS" length="34499980"/>
      <link>https://www.ted.com/talks/katie_hinde_what_we_don_t_know_about_mother_s_milk?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2720</guid>
      <jwplayer:talkId>2720</jwplayer:talkId>
      <pubDate>Tue, 28 Mar 2017 15:07:30 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:09:59</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/KatieHinde_2016W.mp4?apikey=TEDRSS" fileSize="34499980" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/8ccf61eaa86cb971ae1e7c7b5a765e8f39d85d20_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/8ccf61eaa86cb971ae1e7c7b5a765e8f39d85d20_2880x1620.jpg?"/>
    </item>
    <item>
      <title>A young inventor's plan to recycle Styrofoam | Ashton Cofer</title>
      <itunes:author>Ashton Cofer</itunes:author>
      <description>
        <![CDATA[From packing peanuts to disposable coffee cups, each year the US alone produces some two billion pounds of Styrofoam -- none of which can be recycled. Frustrated by this waste of resources and landfill space, Ashton Cofer and his science fair teammates developed a heating treatment to break down used Styrofoam into something useful. Check out their original design, which won both the FIRST LEGO League Global Innovation Award and the Scientific American Innovator Award from Google Science Fair.]]>
      </description>
      <itunes:subtitle>A young inventor's plan to recycle Styrofoam | Ashton Cofer</itunes:subtitle>
      <itunes:summary>
        <![CDATA[From packing peanuts to disposable coffee cups, each year the US alone produces some two billion pounds of Styrofoam -- none of which can be recycled. Frustrated by this waste of resources and landfill space, Ashton Cofer and his science fair teammates developed a heating treatment to break down used Styrofoam into something useful. Check out their original design, which won both the FIRST LEGO League Global Innovation Award and the Scientific American Innovator Award from Google Science Fair.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/AshtonCofer_2016Y.mp4?apikey=TEDRSS" length="20925765"/>
      <link>https://www.ted.com/talks/ashton_cofer_a_young_inventor_s_plan_to_recycle_styrofoam?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2719</guid>
      <jwplayer:talkId>2719</jwplayer:talkId>
      <pubDate>Mon, 27 Mar 2017 15:12:08 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:06:01</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/AshtonCofer_2016Y.mp4?apikey=TEDRSS" fileSize="20925765" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/06a50fa52b0138efd93cb88b83d186bf93c5cd0c_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/06a50fa52b0138efd93cb88b83d186bf93c5cd0c_2880x1620.jpg?"/>
    </item>
    <item>
      <title>3 ways to spot a bad statistic | Mona Chalabi</title>
      <itunes:author>Mona Chalabi</itunes:author>
      <description>
        <![CDATA[Sometimes it's hard to know what statistics are worthy of trust. But we shouldn't count out stats altogether ... instead, we should learn to look behind them. In this delightful, hilarious talk, data journalist Mona Chalabi shares handy tips to help question, interpret and truly understand what the numbers are saying.]]>
      </description>
      <itunes:subtitle>3 ways to spot a bad statistic | Mona Chalabi</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Sometimes it's hard to know what statistics are worthy of trust. But we shouldn't count out stats altogether ... instead, we should learn to look behind them. In this delightful, hilarious talk, data journalist Mona Chalabi shares handy tips to help question, interpret and truly understand what the numbers are saying.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/MonaChalabi_2017S.mp4?apikey=TEDRSS" length="40552136"/>
      <link>https://www.ted.com/talks/mona_chalabi_3_ways_to_spot_a_bad_statistic?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2718</guid>
      <jwplayer:talkId>2718</jwplayer:talkId>
      <pubDate>Fri, 24 Mar 2017 14:48:55 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:11:45</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/MonaChalabi_2017S.mp4?apikey=TEDRSS" fileSize="40552136" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/d0fbe8b884f5572ba9c66e421ca4f9fa7159a19b_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/d0fbe8b884f5572ba9c66e421ca4f9fa7159a19b_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Who would the rest of the world vote for in your country's election? | Simon Anholt</title>
      <itunes:author>Simon Anholt</itunes:author>
      <description>
        <![CDATA[Wish you could vote in another country's election? Simon Anholt unveils the Global Vote, an online platform that lets anybody, anywhere in the world, "vote" in the election of any country on earth (with surprising results).]]>
      </description>
      <itunes:subtitle>Who would the rest of the world vote for in your country's election? | Simon Anholt</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Wish you could vote in another country's election? Simon Anholt unveils the Global Vote, an online platform that lets anybody, anywhere in the world, "vote" in the election of any country on earth (with surprising results).]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/SimonAnholt_2016X.mp4?apikey=TEDRSS" length="51313267"/>
      <link>https://www.ted.com/talks/simon_anholt_how_would_the_rest_of_the_world_vote_in_your_country_s_election?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2714</guid>
      <jwplayer:talkId>2714</jwplayer:talkId>
      <pubDate>Thu, 23 Mar 2017 15:19:43 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:14:55</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/SimonAnholt_2016X.mp4?apikey=TEDRSS" fileSize="51313267" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/c8f5d92fb0ce292b8d90060bc062045ad04235a3_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/c8f5d92fb0ce292b8d90060bc062045ad04235a3_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Why civilians suffer more once a war is over | Margaret Bourdeaux</title>
      <itunes:author>Margaret Bourdeaux</itunes:author>
      <description>
        <![CDATA[In a war, it turns out that violence isn't the biggest killer of civilians. What is? Illness, hunger, poverty -- because war destroys the institutions that keep society running, like utilities, banks, food systems and hospitals. Physician Margaret Bourdeaux proposes a bold approach to post-conflict recovery, setting priorities on what to fix first]]>
      </description>
      <itunes:subtitle>Why civilians suffer more once a war is over | Margaret Bourdeaux</itunes:subtitle>
      <itunes:summary>
        <![CDATA[In a war, it turns out that violence isn't the biggest killer of civilians. What is? Illness, hunger, poverty -- because war destroys the institutions that keep society running, like utilities, banks, food systems and hospitals. Physician Margaret Bourdeaux proposes a bold approach to post-conflict recovery, setting priorities on what to fix first]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/MargaretBourdeaux_2015X.mp4?apikey=TEDRSS" length="49180011"/>
      <link>https://www.ted.com/talks/margaret_bourdeaux_why_civilians_suffer_more_once_a_war_is_over?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2688</guid>
      <jwplayer:talkId>2688</jwplayer:talkId>
      <pubDate>Wed, 22 Mar 2017 15:17:38 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:14:21</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/MargaretBourdeaux_2015X.mp4?apikey=TEDRSS" fileSize="49180011" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/859ceb197283dcbb41afb95e02fc0806b7610402_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/859ceb197283dcbb41afb95e02fc0806b7610402_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Asking for help is a strength, not a weakness | Michele L. Sullivan</title>
      <itunes:author>Michele L. Sullivan</itunes:author>
      <description>
        <![CDATA[We all go through challenges -- some you can see, most you can't, says Michele L. Sullivan. In a talk about perspective, Sullivan shares stories full of wit and wisdom and reminds us that we're all part of each other's support systems. "The only shoes you can walk in are your own," she says. "With compassion, courage and understanding, we can walk together, side by side."]]>
      </description>
      <itunes:subtitle>Asking for help is a strength, not a weakness | Michele L. Sullivan</itunes:subtitle>
      <itunes:summary>
        <![CDATA[We all go through challenges -- some you can see, most you can't, says Michele L. Sullivan. In a talk about perspective, Sullivan shares stories full of wit and wisdom and reminds us that we're all part of each other's support systems. "The only shoes you can walk in are your own," she says. "With compassion, courage and understanding, we can walk together, side by side."]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/MicheleLSullivan_2016W.mp4?apikey=TEDRSS" length="40945304"/>
      <link>https://www.ted.com/talks/michele_l_sullivan_asking_for_help_is_a_strength_not_a_weakness?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2712</guid>
      <jwplayer:talkId>2712</jwplayer:talkId>
      <pubDate>Tue, 21 Mar 2017 14:55:30 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:11:55</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/MicheleLSullivan_2016W.mp4?apikey=TEDRSS" fileSize="40945304" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/e14451f41c08d1eda55938af1a0186e06fcf889b_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/e14451f41c08d1eda55938af1a0186e06fcf889b_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Lifelike simulations that make real-life surgery safer | Peter Weinstock</title>
      <itunes:author>Peter Weinstock</itunes:author>
      <description>
        <![CDATA[Critical care doctor Peter Weinstock shows how surgical teams are using a blend of Hollywood special effects and 3D printing to create amazingly lifelike reproductions of real patients -- so they can practice risky surgeries ahead of time. Think: "Operate twice, cut once." Glimpse the future of surgery in this forward-thinking talk.]]>
      </description>
      <itunes:subtitle>Lifelike simulations that make real-life surgery safer | Peter Weinstock</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Critical care doctor Peter Weinstock shows how surgical teams are using a blend of Hollywood special effects and 3D printing to create amazingly lifelike reproductions of real patients -- so they can practice risky surgeries ahead of time. Think: "Operate twice, cut once." Glimpse the future of surgery in this forward-thinking talk.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/PeterWeinstock_2016X.mp4?apikey=TEDRSS" length="58302796"/>
      <link>https://www.ted.com/talks/peter_weinstock_lifelike_simulations_that_make_real_life_surgery_safer?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2711</guid>
      <jwplayer:talkId>2711</jwplayer:talkId>
      <pubDate>Mon, 20 Mar 2017 14:57:11 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:16:58</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/PeterWeinstock_2016X.mp4?apikey=TEDRSS" fileSize="58302796" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/5e9bba9dfcd7dd8208bcc7babe14ae2df97ba7c8_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/5e9bba9dfcd7dd8208bcc7babe14ae2df97ba7c8_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Inside America's dead shopping malls | Dan Bell</title>
      <itunes:author>Dan Bell</itunes:author>
      <description>
        <![CDATA[What happens when a mall falls into ruin? Filmmaker Dan Bell guides us through abandoned monoliths of merchandise, providing a surprisingly funny and lyrical commentary on consumerism, youth culture and the inspiration we can find in decay.]]>
      </description>
      <itunes:subtitle>Inside America's dead shopping malls | Dan Bell</itunes:subtitle>
      <itunes:summary>
        <![CDATA[What happens when a mall falls into ruin? Filmmaker Dan Bell guides us through abandoned monoliths of merchandise, providing a surprisingly funny and lyrical commentary on consumerism, youth culture and the inspiration we can find in decay.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/DanBell_2016X.mp4?apikey=TEDRSS" length="40936716"/>
      <link>https://www.ted.com/talks/dan_bell_inside_america_s_dead_shopping_malls?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2713</guid>
      <jwplayer:talkId>2713</jwplayer:talkId>
      <pubDate>Fri, 17 Mar 2017 15:51:46 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:11:53</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/DanBell_2016X.mp4?apikey=TEDRSS" fileSize="40936716" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/1754ca1558bfb616d757d987448d7afe32cf32cc_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/1754ca1558bfb616d757d987448d7afe32cf32cc_2880x1620.jpg?"/>
    </item>
    <item>
      <title>"Turceasca" |  Silk Road Ensemble</title>
      <itunes:author> Silk Road Ensemble</itunes:author>
      <description>
        <![CDATA[Grammy-winning Silk Road Ensemble display their eclectic convergence of violin, clarinet, bass, drums and more in this energetic rendition of the traditional Roma tune, "Turceasca."]]>
      </description>
      <itunes:subtitle>"Turceasca" |  Silk Road Ensemble</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Grammy-winning Silk Road Ensemble display their eclectic convergence of violin, clarinet, bass, drums and more in this energetic rendition of the traditional Roma tune, "Turceasca."]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/SilkRoadEnsemble_2016.mp4?apikey=TEDRSS" length="22420965"/>
      <link>https://www.ted.com/talks/silk_road_ensemble_turceasca?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2710</guid>
      <jwplayer:talkId>2710</jwplayer:talkId>
      <pubDate>Fri, 17 Mar 2017 14:00:15 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:06:29</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/SilkRoadEnsemble_2016.mp4?apikey=TEDRSS" fileSize="22420965" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/ca3c55b7de28922f919bf9231c2216c47794b1a6_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/ca3c55b7de28922f919bf9231c2216c47794b1a6_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Should we simplify spelling? | Karina Galperin</title>
      <itunes:author>Karina Galperin</itunes:author>
      <description>
        <![CDATA[How much energy and brain power do we devote to learning how to spell? Language evolves over time, and with it the way we spell -- is it worth it to spend so much time memorizing rules that are filled with endless exceptions? Literary scholar Karina Galperin suggests that it may be time for an update in the way we think about and record language. (In Spanish with English subtitles.)]]>
      </description>
      <itunes:subtitle>Should we simplify spelling? | Karina Galperin</itunes:subtitle>
      <itunes:summary>
        <![CDATA[How much energy and brain power do we devote to learning how to spell? Language evolves over time, and with it the way we spell -- is it worth it to spend so much time memorizing rules that are filled with endless exceptions? Literary scholar Karina Galperin suggests that it may be time for an update in the way we think about and record language. (In Spanish with English subtitles.)]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/KarinaGalperin_2015X.mp4?apikey=TEDRSS" length="55503678"/>
      <link>https://www.ted.com/talks/karina_galperin_why_don_t_we_write_words_the_way_pronounce_them?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2692</guid>
      <jwplayer:talkId>2692</jwplayer:talkId>
      <pubDate>Thu, 16 Mar 2017 15:00:10 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:16:13</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/KarinaGalperin_2015X.mp4?apikey=TEDRSS" fileSize="55503678" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/803453a1398a2619734586bb230ed932ad6f683f_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/803453a1398a2619734586bb230ed932ad6f683f_2880x1620.jpg?"/>
    </item>
    <item>
      <title>What young women believe about their own sexual pleasure | Peggy Orenstein</title>
      <itunes:author>Peggy Orenstein</itunes:author>
      <description>
        <![CDATA[Why do girls feel empowered to engage in sexual activity but not to enjoy it? For three years, author Peggy Orenstein interviewed girls ages 15 to 20 about their attitudes toward and experiences of sex. She discusses the pleasure that's largely missing from their sexual encounters and calls on us to close the "orgasm gap" by talking candidly with our girls from an early age about sex, bodies, pleasure and intimacy.]]>
      </description>
      <itunes:subtitle>What young women believe about their own sexual pleasure | Peggy Orenstein</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Why do girls feel empowered to engage in sexual activity but not to enjoy it? For three years, author Peggy Orenstein interviewed girls ages 15 to 20 about their attitudes toward and experiences of sex. She discusses the pleasure that's largely missing from their sexual encounters and calls on us to close the "orgasm gap" by talking candidly with our girls from an early age about sex, bodies, pleasure and intimacy.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/PeggyOrenstein_2016W.mp4?apikey=TEDRSS" length="58231029"/>
      <link>https://www.ted.com/talks/peggy_orenstein_what_young_women_believe_about_their_own_sexual_pleasure?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2708</guid>
      <jwplayer:talkId>2708</jwplayer:talkId>
      <pubDate>Wed, 15 Mar 2017 15:00:14 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:17:00</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/PeggyOrenstein_2016W.mp4?apikey=TEDRSS" fileSize="58231029" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/346ec05e16531c240158728114661bef330ca3ef_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/346ec05e16531c240158728114661bef330ca3ef_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Adventures of an asteroid hunter | Carrie Nugent</title>
      <itunes:author>Carrie Nugent</itunes:author>
      <description>
        <![CDATA[TED Fellow Carrie Nugent is an asteroid hunter -- part of a group of scientists working to discover and catalog our oldest and most numerous cosmic neighbors. Why keep an eye out for asteroids? In this short, fact-filled talk, Nugent explains how their awesome impacts have shaped our planet, and how finding them at the right time could mean nothing less than saving life on Earth.]]>
      </description>
      <itunes:subtitle>Adventures of an asteroid hunter | Carrie Nugent</itunes:subtitle>
      <itunes:summary>
        <![CDATA[TED Fellow Carrie Nugent is an asteroid hunter -- part of a group of scientists working to discover and catalog our oldest and most numerous cosmic neighbors. Why keep an eye out for asteroids? In this short, fact-filled talk, Nugent explains how their awesome impacts have shaped our planet, and how finding them at the right time could mean nothing less than saving life on Earth.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/CarrieNugent_2016U.mp4?apikey=TEDRSS" length="21038137"/>
      <link>https://www.ted.com/talks/carrie_nugent_adventures_of_an_asteroid_hunter?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2491</guid>
      <jwplayer:talkId>2491</jwplayer:talkId>
      <pubDate>Tue, 14 Mar 2017 14:38:51 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:06:06</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/CarrieNugent_2016U.mp4?apikey=TEDRSS" fileSize="21038137" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/a8dbff8cfccb989af849c15247d98d81283915c8_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/a8dbff8cfccb989af849c15247d98d81283915c8_2880x1620.jpg?"/>
    </item>
    <item>
      <title>A burial practice that nourishes the planet | Caitlin Doughty</title>
      <itunes:author>Caitlin Doughty</itunes:author>
      <description>
        <![CDATA[Here's a question we all have to answer sooner or later: What do you want to happen to your body when you die? Funeral director Caitlin Doughty explores new ways to prepare us for inevitable mortality. In this thoughtful talk, learn more about ideas for burial (like "recomposting" and "conservation burial") that return our bodies back to the earth in an eco-friendly, humble and self-aware way.]]>
      </description>
      <itunes:subtitle>A burial practice that nourishes the planet | Caitlin Doughty</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Here's a question we all have to answer sooner or later: What do you want to happen to your body when you die? Funeral director Caitlin Doughty explores new ways to prepare us for inevitable mortality. In this thoughtful talk, learn more about ideas for burial (like "recomposting" and "conservation burial") that return our bodies back to the earth in an eco-friendly, humble and self-aware way.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/CaitlinDoughty_2016P.mp4?apikey=TEDRSS" length="40877357"/>
      <link>https://www.ted.com/talks/caitlin_doughty_a_burial_practice_that_nourishes_the_planet?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2707</guid>
      <jwplayer:talkId>2707</jwplayer:talkId>
      <pubDate>Mon, 13 Mar 2017 14:59:37 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:11:54</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/CaitlinDoughty_2016P.mp4?apikey=TEDRSS" fileSize="40877357" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/2eb99d9f7343faae8b7af40447b4992dd5a9325c_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/2eb99d9f7343faae8b7af40447b4992dd5a9325c_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Beautiful new words to describe obscure emotions | John Koenig</title>
      <itunes:author>John Koenig</itunes:author>
      <description>
        <![CDATA[John Koenig loves finding words that express our unarticulated feelings -- like "lachesism," the hunger for disaster, and "sonder," the realization that everyone else's lives are as complex and unknowable as our own. Here, he meditates on the meaning we assign to words and how these meanings latch onto us.]]>
      </description>
      <itunes:subtitle>Beautiful new words to describe obscure emotions | John Koenig</itunes:subtitle>
      <itunes:summary>
        <![CDATA[John Koenig loves finding words that express our unarticulated feelings -- like "lachesism," the hunger for disaster, and "sonder," the realization that everyone else's lives are as complex and unknowable as our own. Here, he meditates on the meaning we assign to words and how these meanings latch onto us.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/JohnKoenig_2016X.mp4?apikey=TEDRSS" length="26028699"/>
      <link>https://www.ted.com/talks/john_koenig_beautiful_new_words_to_describe_obscure_emotions?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2709</guid>
      <jwplayer:talkId>2709</jwplayer:talkId>
      <pubDate>Fri, 10 Mar 2017 16:01:47 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:07:28</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/JohnKoenig_2016X.mp4?apikey=TEDRSS" fileSize="26028699" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/8563d3f14c361cf84c0386a8b96d457e9e9d808e_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/8563d3f14c361cf84c0386a8b96d457e9e9d808e_2880x1620.jpg?"/>
    </item>
    <item>
      <title>How I'm fighting bias in algorithms | Joy Buolamwini</title>
      <itunes:author>Joy Buolamwini</itunes:author>
      <description>
        <![CDATA[MIT grad student Joy Buolamwini was working with facial analysis software when she noticed a problem: the software didn't detect her face -- because the people who coded the algorithm hadn't taught it to identify a broad range of skin tones and facial structures. Now she's on a mission to fight bias in machine learning, a phenomenon she calls the "coded gaze." It's an eye-opening talk about the need for accountability in coding ... as algorithms take over more and more aspects of our lives.]]>
      </description>
      <itunes:subtitle>How I'm fighting bias in algorithms | Joy Buolamwini</itunes:subtitle>
      <itunes:summary>
        <![CDATA[MIT grad student Joy Buolamwini was working with facial analysis software when she noticed a problem: the software didn't detect her face -- because the people who coded the algorithm hadn't taught it to identify a broad range of skin tones and facial structures. Now she's on a mission to fight bias in machine learning, a phenomenon she calls the "coded gaze." It's an eye-opening talk about the need for accountability in coding ... as algorithms take over more and more aspects of our lives.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/JoyBuolamwini_2016X.mp4?apikey=TEDRSS" length="42227073"/>
      <link>https://www.ted.com/talks/joy_buolamwini_how_i_m_fighting_bias_in_algorithms?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2705</guid>
      <jwplayer:talkId>2705</jwplayer:talkId>
      <pubDate>Thu, 09 Mar 2017 16:08:53 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:08:44</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/JoyBuolamwini_2016X.mp4?apikey=TEDRSS" fileSize="42227073" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/dbd2137053b8698a43c1a169b1789c9001222ea7_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/dbd2137053b8698a43c1a169b1789c9001222ea7_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Why women should tell the stories of humanity | Jude Kelly</title>
      <itunes:author>Jude Kelly</itunes:author>
      <description>
        <![CDATA[For many centuries (and for many reasons) critically acclaimed creative genius has generally come from a male perspective. As theater director Jude Kelly points out in this passionately reasoned talk, that skew affects how we interpret even non-fictional women's stories and rights. She thinks there's a more useful, more inclusive way to look at the world, and she calls on artists -- women and men -- to paint, draw, write about, film and imagine a gender-equal society.]]>
      </description>
      <itunes:subtitle>Why women should tell the stories of humanity | Jude Kelly</itunes:subtitle>
      <itunes:summary>
        <![CDATA[For many centuries (and for many reasons) critically acclaimed creative genius has generally come from a male perspective. As theater director Jude Kelly points out in this passionately reasoned talk, that skew affects how we interpret even non-fictional women's stories and rights. She thinks there's a more useful, more inclusive way to look at the world, and she calls on artists -- women and men -- to paint, draw, write about, film and imagine a gender-equal society.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/JudeKelly_2016W.mp4?apikey=TEDRSS" length="59342134"/>
      <link>https://www.ted.com/talks/jude_kelly_why_women_should_tell_the_stories_of_humanity?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2706</guid>
      <jwplayer:talkId>2706</jwplayer:talkId>
      <pubDate>Wed, 08 Mar 2017 15:53:57 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:13:22</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/JudeKelly_2016W.mp4?apikey=TEDRSS" fileSize="59342134" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/00c97f7d52060c0df6c90fad9037dbf1e7efee39_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/00c97f7d52060c0df6c90fad9037dbf1e7efee39_2880x1620.jpg?"/>
    </item>
    <item>
      <title>To raise brave girls, encourage adventure | Caroline Paul</title>
      <itunes:author>Caroline Paul</itunes:author>
      <description>
        <![CDATA[Gutsy girls skateboard, climb trees, clamber around, fall down, scrape their knees, get right back up -- and grow up to be brave women. Learn how to spark a little productive risk-taking and raise confident girls with stories and advice from firefighter, paraglider and all-around adventurer Caroline Paul.]]>
      </description>
      <itunes:subtitle>To raise brave girls, encourage adventure | Caroline Paul</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Gutsy girls skateboard, climb trees, clamber around, fall down, scrape their knees, get right back up -- and grow up to be brave women. Learn how to spark a little productive risk-taking and raise confident girls with stories and advice from firefighter, paraglider and all-around adventurer Caroline Paul.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/CarolinePaul_2016W.mp4?apikey=TEDRSS" length="57478545"/>
      <link>https://www.ted.com/talks/caroline_paul_to_raise_brave_girls_encourage_adventure?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2704</guid>
      <jwplayer:talkId>2704</jwplayer:talkId>
      <pubDate>Tue, 07 Mar 2017 15:56:54 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:12:41</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/CarolinePaul_2016W.mp4?apikey=TEDRSS" fileSize="57478545" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/595531755191891ba14e4ee36145bf04c6b3452a_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/595531755191891ba14e4ee36145bf04c6b3452a_2880x1620.jpg?"/>
    </item>
    <item>
      <title>I grew up in the Westboro Baptist Church. Here's why I left | Megan Phelps-Roper</title>
      <itunes:author>Megan Phelps-Roper</itunes:author>
      <description>
        <![CDATA[What's it like to grow up within a group of people who exult in demonizing ... everyone else? Megan Phelps-Roper shares details of life inside America's most controversial church and describes how conversations on Twitter were key to her decision to leave it. In this extraordinary talk, she shares her personal experience of extreme polarization, along with some sharp ways we can learn to successfully engage across ideological lines.]]>
      </description>
      <itunes:subtitle>I grew up in the Westboro Baptist Church. Here's why I left | Megan Phelps-Roper</itunes:subtitle>
      <itunes:summary>
        <![CDATA[What's it like to grow up within a group of people who exult in demonizing ... everyone else? Megan Phelps-Roper shares details of life inside America's most controversial church and describes how conversations on Twitter were key to her decision to leave it. In this extraordinary talk, she shares her personal experience of extreme polarization, along with some sharp ways we can learn to successfully engage across ideological lines.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/MeganPhelpsRoper_2017S.mp4?apikey=TEDRSS" length="66126848"/>
      <link>https://www.ted.com/talks/megan_phelps_roper_i_grew_up_in_the_westboro_baptist_church_here_s_why_i_left?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2703</guid>
      <jwplayer:talkId>2703</jwplayer:talkId>
      <pubDate>Mon, 06 Mar 2017 16:05:05 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:15:17</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/MeganPhelpsRoper_2017S.mp4?apikey=TEDRSS" fileSize="66126848" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/50152d92514ec9a6eef99217a209b1acd196bf6a_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/50152d92514ec9a6eef99217a209b1acd196bf6a_2880x1620.jpg?"/>
    </item>
    <item>
      <title>A scientific approach to the paranormal | Carrie Poppy</title>
      <itunes:author>Carrie Poppy</itunes:author>
      <description>
        <![CDATA[What's haunting Carrie Poppy? Is it ghosts or something worse? In this talk, the investigative journalist narrates her encounter with a spooky feeling you'll want to warn your friends about and explains why we need science to deal with paranormal activity. ]]>
      </description>
      <itunes:subtitle>A scientific approach to the paranormal | Carrie Poppy</itunes:subtitle>
      <itunes:summary>
        <![CDATA[What's haunting Carrie Poppy? Is it ghosts or something worse? In this talk, the investigative journalist narrates her encounter with a spooky feeling you'll want to warn your friends about and explains why we need science to deal with paranormal activity. ]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/CarriePoppy_2016X.mp4?apikey=TEDRSS" length="44927719"/>
      <link>https://www.ted.com/talks/carrie_poppy_a_scientific_approach_to_the_paranormal?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2702</guid>
      <jwplayer:talkId>2702</jwplayer:talkId>
      <pubDate>Fri, 03 Mar 2017 16:38:07 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:12:58</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/CarriePoppy_2016X.mp4?apikey=TEDRSS" fileSize="44927719" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/7782f8e1a286177d5daf35eb2978d0f5f09c94e3_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/7782f8e1a286177d5daf35eb2978d0f5f09c94e3_2880x1620.jpg?"/>
    </item>
    <item>
      <title>"Rollercoaster" | Sara Ramirez</title>
      <itunes:author>Sara Ramirez</itunes:author>
      <description>
        <![CDATA[Singer, songwriter and actress Sara Ramirez is a woman of many talents. Joined by Michael Pemberton on guitar, Ramirez sings of opportunity, wisdom and the highs and lows of life in this live performance of her song, "Rollercoaster."]]>
      </description>
      <itunes:subtitle>"Rollercoaster" | Sara Ramirez</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Singer, songwriter and actress Sara Ramirez is a woman of many talents. Joined by Michael Pemberton on guitar, Ramirez sings of opportunity, wisdom and the highs and lows of life in this live performance of her song, "Rollercoaster."]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/SaraRamirez_2015P.mp4?apikey=TEDRSS" length="33400906"/>
      <link>https://www.ted.com/talks/sara_ramirez_and_michael_pemberton_rollercoaster?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2701</guid>
      <jwplayer:talkId>2701</jwplayer:talkId>
      <pubDate>Fri, 03 Mar 2017 11:56:20 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:04:59</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/SaraRamirez_2015P.mp4?apikey=TEDRSS" fileSize="33400906" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/c9aa15ff881dc0bea4f0e98eb1f75f75df3622e1_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/c9aa15ff881dc0bea4f0e98eb1f75f75df3622e1_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Stories from a home for terminally ill children | Kathy Hull</title>
      <itunes:author>Kathy Hull</itunes:author>
      <description>
        <![CDATA[To honor and celebrate young lives cut short, Kathy Hull founded the first freestanding pediatric palliative care facility in the United States, the George Mark Children's House. Its mission: to give terminally ill children and their families a peaceful place to say goodbye. She shares stories brimming with wisdom, joy, imagination and heartbreaking loss.]]>
      </description>
      <itunes:subtitle>Stories from a home for terminally ill children | Kathy Hull</itunes:subtitle>
      <itunes:summary>
        <![CDATA[To honor and celebrate young lives cut short, Kathy Hull founded the first freestanding pediatric palliative care facility in the United States, the George Mark Children's House. Its mission: to give terminally ill children and their families a peaceful place to say goodbye. She shares stories brimming with wisdom, joy, imagination and heartbreaking loss.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/KathyHull_2016W.mp4?apikey=TEDRSS" length="64327272"/>
      <link>https://www.ted.com/talks/kathy_hull_stories_from_a_home_for_terminally_ill_children?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2698</guid>
      <jwplayer:talkId>2698</jwplayer:talkId>
      <pubDate>Thu, 02 Mar 2017 15:54:00 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:15:18</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/KathyHull_2016W.mp4?apikey=TEDRSS" fileSize="64327272" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/333ff47afa6f6fad2633a2adc7149623b2d69165_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/333ff47afa6f6fad2633a2adc7149623b2d69165_2880x1620.jpg?"/>
    </item>
    <item>
      <title>What I learned from 2,000 obituaries | Lux Narayan</title>
      <itunes:author>Lux Narayan</itunes:author>
      <description>
        <![CDATA[Lux Narayan starts his day with scrambled eggs and the question: "Who died today?" Why? By analyzing 2,000 New York Times obituaries over a 20-month period, Narayan gleaned, in just a few words, what achievement looks like over a lifetime. Here he shares what those immortalized in print can teach us about a life well lived.]]>
      </description>
      <itunes:subtitle>What I learned from 2,000 obituaries | Lux Narayan</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Lux Narayan starts his day with scrambled eggs and the question: "Who died today?" Why? By analyzing 2,000 New York Times obituaries over a 20-month period, Narayan gleaned, in just a few words, what achievement looks like over a lifetime. Here he shares what those immortalized in print can teach us about a life well lived.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/LuxNarayan_2017S.mp4?apikey=TEDRSS" length="33959449"/>
      <link>https://www.ted.com/talks/lux_narayan_what_i_learned_from_2_000_obituaries?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2697</guid>
      <jwplayer:talkId>2697</jwplayer:talkId>
      <pubDate>Wed, 01 Mar 2017 16:05:49 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:06:08</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/LuxNarayan_2017S.mp4?apikey=TEDRSS" fileSize="33959449" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/a7529af95a5fcb9718e959100ba6e7e81fce34f0_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/a7529af95a5fcb9718e959100ba6e7e81fce34f0_2880x1620.jpg?"/>
    </item>
    <item>
      <title>This app makes it fun to pick up litter | Jeff Kirschner</title>
      <itunes:author>Jeff Kirschner</itunes:author>
      <description>
        <![CDATA[The earth is a big place to keep clean. With Litterati -- an app for users to identify, collect and geotag the world's litter -- TED Resident Jeff Kirschner has created a community that's crowdsource-cleaning the planet. After tracking trash in more than 100 countries, Kirschner hopes to use the data he's collected to work with brands and organizations to stop litter before it reaches the ground.]]>
      </description>
      <itunes:subtitle>This app makes it fun to pick up litter | Jeff Kirschner</itunes:subtitle>
      <itunes:summary>
        <![CDATA[The earth is a big place to keep clean. With Litterati -- an app for users to identify, collect and geotag the world's litter -- TED Resident Jeff Kirschner has created a community that's crowdsource-cleaning the planet. After tracking trash in more than 100 countries, Kirschner hopes to use the data he's collected to work with brands and organizations to stop litter before it reaches the ground.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/JeffKirschner_2016S.mp4?apikey=TEDRSS" length="35437928"/>
      <link>https://www.ted.com/talks/jeff_kirschner_this_app_makes_it_fun_to_pick_up_litter?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2696</guid>
      <jwplayer:talkId>2696</jwplayer:talkId>
      <pubDate>Tue, 28 Feb 2017 16:04:41 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:06:10</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/JeffKirschner_2016S.mp4?apikey=TEDRSS" fileSize="35437928" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/8c0bc0c56ed23d48bf9a89421c8507dfaf4d995e_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/8c0bc0c56ed23d48bf9a89421c8507dfaf4d995e_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Smelfies, and other experiments in synthetic biology | Ani Liu</title>
      <itunes:author>Ani Liu</itunes:author>
      <description>
        <![CDATA[What if you could take a smell selfie, a smelfie? What if you had a lipstick that caused plants to grow where you kiss? Ani Liu explores the intersection of technology and sensory perception, and her work is wedged somewhere between science, design and art. In this swift, smart talk, she shares dreams, wonderings and experiments, asking: What happens when science fiction becomes science fact?]]>
      </description>
      <itunes:subtitle>Smelfies, and other experiments in synthetic biology | Ani Liu</itunes:subtitle>
      <itunes:summary>
        <![CDATA[What if you could take a smell selfie, a smelfie? What if you had a lipstick that caused plants to grow where you kiss? Ani Liu explores the intersection of technology and sensory perception, and her work is wedged somewhere between science, design and art. In this swift, smart talk, she shares dreams, wonderings and experiments, asking: What happens when science fiction becomes science fact?]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/AniLiu_2016X.mp4?apikey=TEDRSS" length="25590189"/>
      <link>https://www.ted.com/talks/ani_liu_smelfies_and_other_experiments_in_synthetic_biology?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2700</guid>
      <jwplayer:talkId>2700</jwplayer:talkId>
      <pubDate>Mon, 27 Feb 2017 15:59:48 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:07:20</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/AniLiu_2016X.mp4?apikey=TEDRSS" fileSize="25590189" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/072700321bff01740f3ad3f5631dddc406a971d3_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/072700321bff01740f3ad3f5631dddc406a971d3_2880x1620.jpg?"/>
    </item>
    <item>
      <title>The data behind Hollywood's sexism | Stacy Smith</title>
      <itunes:author>Stacy Smith</itunes:author>
      <description>
        <![CDATA[Where are all the women and girls in film? Social scientist Stacy Smith analyzes how the media underrepresents and portrays women -- and the potentially destructive effects those portrayals have on viewers. She shares hard data behind gender bias in Hollywood, where on-screen males outnumber females three to one (and behind-the-camera workers fare even worse.)]]>
      </description>
      <itunes:subtitle>The data behind Hollywood's sexism | Stacy Smith</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Where are all the women and girls in film? Social scientist Stacy Smith analyzes how the media underrepresents and portrays women -- and the potentially destructive effects those portrayals have on viewers. She shares hard data behind gender bias in Hollywood, where on-screen males outnumber females three to one (and behind-the-camera workers fare even worse.)]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/StacySmith_2016W.mp4?apikey=TEDRSS" length="62616561"/>
      <link>https://www.ted.com/talks/stacy_smith_the_data_behind_hollywood_s_sexism?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2694</guid>
      <jwplayer:talkId>2694</jwplayer:talkId>
      <pubDate>Fri, 24 Feb 2017 15:50:04 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:15:44</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/StacySmith_2016W.mp4?apikey=TEDRSS" fileSize="62616561" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/aae82a50e897dbeea4237bd632f7a5da09bc6a81_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/aae82a50e897dbeea4237bd632f7a5da09bc6a81_2880x1620.jpg?"/>
    </item>
    <item>
      <title>A few ways to fix a government | Charity Wayua</title>
      <itunes:author>Charity Wayua</itunes:author>
      <description>
        <![CDATA[Charity Wayua put her skills as a cancer researcher to use on an unlikely patient: the government of her native Kenya. She shares how she helped her government drastically improve its process for opening up new businesses, a crucial part of economic health and growth, leading to new investments and a World Bank recognition as a top reformer.]]>
      </description>
      <itunes:subtitle>A few ways to fix a government | Charity Wayua</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Charity Wayua put her skills as a cancer researcher to use on an unlikely patient: the government of her native Kenya. She shares how she helped her government drastically improve its process for opening up new businesses, a crucial part of economic health and growth, leading to new investments and a World Bank recognition as a top reformer.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/CharityWayua_2016S.mp4?apikey=TEDRSS" length="51418047"/>
      <link>https://www.ted.com/talks/charity_wayua_a_few_ways_to_fix_an_ailing_government?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2653</guid>
      <jwplayer:talkId>2653</jwplayer:talkId>
      <pubDate>Thu, 23 Feb 2017 15:49:58 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:11:51</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/CharityWayua_2016S.mp4?apikey=TEDRSS" fileSize="51418047" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/42f75324b7d4581825456adf9e05c1543f12e7d8_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/42f75324b7d4581825456adf9e05c1543f12e7d8_2880x1620.jpg?"/>
    </item>
    <item>
      <title>A robot that eats pollution | Jonathan Rossiter</title>
      <itunes:author>Jonathan Rossiter</itunes:author>
      <description>
        <![CDATA[Meet the "Row-bot," a robot that cleans up pollution and generates the electricity needed to power itself by swallowing dirty water. Roboticist Jonathan Rossiter explains how this special swimming machine, which uses a microbial fuel cell to neutralize algal blooms and oil slicks, could be a precursor to biodegradable, autonomous pollution-fighting robots.]]>
      </description>
      <itunes:subtitle>A robot that eats pollution | Jonathan Rossiter</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Meet the "Row-bot," a robot that cleans up pollution and generates the electricity needed to power itself by swallowing dirty water. Roboticist Jonathan Rossiter explains how this special swimming machine, which uses a microbial fuel cell to neutralize algal blooms and oil slicks, could be a precursor to biodegradable, autonomous pollution-fighting robots.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/JonathanRossiter_2016X.mp4?apikey=TEDRSS" length="57638573"/>
      <link>https://www.ted.com/talks/jonathan_rossiter_a_robot_that_eats_pollution?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2690</guid>
      <jwplayer:talkId>2690</jwplayer:talkId>
      <pubDate>Wed, 22 Feb 2017 16:17:33 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:14:10</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/JonathanRossiter_2016X.mp4?apikey=TEDRSS" fileSize="57638573" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/e49724e64405b5ac5ebe4a17b7849da696cda485_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/e49724e64405b5ac5ebe4a17b7849da696cda485_2880x1620.jpg?"/>
    </item>
    <item>
      <title>The racial politics of time | Brittney Cooper</title>
      <itunes:author>Brittney Cooper</itunes:author>
      <description>
        <![CDATA[Cultural theorist Brittney Cooper examines racism through the lens of time, showing us how historically it has been stolen from people of color, resulting in lost moments of joy and connection, lost years of healthy quality of life and the delay of progress. A candid, thought-provoking take on history and race that may make you reconsider your understanding of time, and your place in it.]]>
      </description>
      <itunes:subtitle>The racial politics of time | Brittney Cooper</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Cultural theorist Brittney Cooper examines racism through the lens of time, showing us how historically it has been stolen from people of color, resulting in lost moments of joy and connection, lost years of healthy quality of life and the delay of progress. A candid, thought-provoking take on history and race that may make you reconsider your understanding of time, and your place in it.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/BrittneyCooper_2016W.mp4?apikey=TEDRSS" length="58307139"/>
      <link>https://www.ted.com/talks/brittney_cooper_the_racial_politics_of_time?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2691</guid>
      <jwplayer:talkId>2691</jwplayer:talkId>
      <pubDate>Tue, 21 Feb 2017 16:08:54 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:12:29</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/BrittneyCooper_2016W.mp4?apikey=TEDRSS" fileSize="58307139" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/affb49391e46ad96c3ad2a9f19cf4b8eb1493689_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/affb49391e46ad96c3ad2a9f19cf4b8eb1493689_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Nationalism vs. globalism: the new political divide | Yuval Noah Harari</title>
      <itunes:author>Yuval Noah Harari</itunes:author>
      <description>
        <![CDATA[How do we make sense of today's political divisions? In a wide-ranging conversation full of insight, historian Yuval Harari places our current turmoil in a broader context, against the ongoing disruption of our technology, climate, media -- even our notion of what humanity is for. This is the first of a series of TED Dialogues, seeking a thoughtful response to escalating political divisiveness. Make time (just over an hour) for this fascinating discussion between Harari and TED curator Chris Anderson.]]>
      </description>
      <itunes:subtitle>Nationalism vs. globalism: the new political divide | Yuval Noah Harari</itunes:subtitle>
      <itunes:summary>
        <![CDATA[How do we make sense of today's political divisions? In a wide-ranging conversation full of insight, historian Yuval Harari places our current turmoil in a broader context, against the ongoing disruption of our technology, climate, media -- even our notion of what humanity is for. This is the first of a series of TED Dialogues, seeking a thoughtful response to escalating political divisiveness. Make time (just over an hour) for this fascinating discussion between Harari and TED curator Chris Anderson.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/YuvalNoahHarari_2017S.mp4?apikey=TEDRSS" length="216723621"/>
      <link>https://www.ted.com/talks/yuval_noah_harari_nationalism_vs_globalism_the_new_political_divide?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2695</guid>
      <jwplayer:talkId>2695</jwplayer:talkId>
      <pubDate>Mon, 20 Feb 2017 15:35:24 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>01:00:08</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/YuvalNoahHarari_2017S.mp4?apikey=TEDRSS" fileSize="216723621" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/3e43a1c97f4668cc86486306ccf018e994241646_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/3e43a1c97f4668cc86486306ccf018e994241646_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Don't fear superintelligent AI | Grady Booch</title>
      <itunes:author>Grady Booch</itunes:author>
      <description>
        <![CDATA[New tech spawns new anxieties, says scientist and philosopher Grady Booch, but we don't need to be afraid an all-powerful, unfeeling AI. Booch allays our worst (sci-fi induced) fears about superintelligent computers by explaining how we'll teach, not program, them to share our human values. Rather than worry about an unlikely existential threat, he urges us to consider how artificial intelligence will enhance human life.]]>
      </description>
      <itunes:subtitle>Don't fear superintelligent AI | Grady Booch</itunes:subtitle>
      <itunes:summary>
        <![CDATA[New tech spawns new anxieties, says scientist and philosopher Grady Booch, but we don't need to be afraid an all-powerful, unfeeling AI. Booch allays our worst (sci-fi induced) fears about superintelligent computers by explaining how we'll teach, not program, them to share our human values. Rather than worry about an unlikely existential threat, he urges us to consider how artificial intelligence will enhance human life.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/GradyBooch_2016S.mp4?apikey=TEDRSS" length="47277237"/>
      <link>https://www.ted.com/talks/grady_booch_don_t_fear_superintelligence?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2689</guid>
      <jwplayer:talkId>2689</jwplayer:talkId>
      <pubDate>Fri, 17 Feb 2017 16:09:46 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:10:20</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/GradyBooch_2016S.mp4?apikey=TEDRSS" fileSize="47277237" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/e6b332a04f02523d85d3758f4db1aa2c29d4cd8c_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/e6b332a04f02523d85d3758f4db1aa2c29d4cd8c_2880x1620.jpg?"/>
    </item>
    <item>
      <title>How jails extort the poor | Salil Dudani</title>
      <itunes:author>Salil Dudani</itunes:author>
      <description>
        <![CDATA[Why do we jail people for being poor? Today, half a million Americans are in jail only because they can't afford to post bail, and still more are locked up because they can't pay their debt to the court, sometimes for things as minor as unpaid parking tickets. Salil Dudani shares stories from individuals who have experienced debtors' prison in Ferguson, Missouri, challenging us to think differently about how we punish the poor and marginalized.]]>
      </description>
      <itunes:subtitle>How jails extort the poor | Salil Dudani</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Why do we jail people for being poor? Today, half a million Americans are in jail only because they can't afford to post bail, and still more are locked up because they can't pay their debt to the court, sometimes for things as minor as unpaid parking tickets. Salil Dudani shares stories from individuals who have experienced debtors' prison in Ferguson, Missouri, challenging us to think differently about how we punish the poor and marginalized.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/SalilDudani_2016X.mp4?apikey=TEDRSS" length="52276338"/>
      <link>https://www.ted.com/talks/salil_dudani_how_jails_extort_the_poor?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2675</guid>
      <jwplayer:talkId>2675</jwplayer:talkId>
      <pubDate>Thu, 16 Feb 2017 16:05:36 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:12:43</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/SalilDudani_2016X.mp4?apikey=TEDRSS" fileSize="52276338" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/4658539c6f725bfe3d2681f733f16237ac2f1527_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/4658539c6f725bfe3d2681f733f16237ac2f1527_2880x1620.jpg?"/>
    </item>
    <item>
      <title>3 ways to fix a broken news industry | Lara Setrakian</title>
      <itunes:author>Lara Setrakian</itunes:author>
      <description>
        <![CDATA[Something is very wrong with the news industry. Trust in the media has hit an all-time low; we're inundated with sensationalist stories, and consistent, high-quality reporting is scarce, says journalist Lara Setrakian. She shares three ways we can fix the news to better inform all of us about the complex issues of our time.]]>
      </description>
      <itunes:subtitle>3 ways to fix a broken news industry | Lara Setrakian</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Something is very wrong with the news industry. Trust in the media has hit an all-time low; we're inundated with sensationalist stories, and consistent, high-quality reporting is scarce, says journalist Lara Setrakian. She shares three ways we can fix the news to better inform all of us about the complex issues of our time.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/LaraSetrakian_2017S.mp4?apikey=TEDRSS" length="43983598"/>
      <link>https://www.ted.com/talks/lara_setrakian_3_ways_to_fix_a_broken_news_industry?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2687</guid>
      <jwplayer:talkId>2687</jwplayer:talkId>
      <pubDate>Wed, 15 Feb 2017 16:07:49 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:08:37</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/LaraSetrakian_2017S.mp4?apikey=TEDRSS" fileSize="43983598" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/2973ebdecf9c4e04a6bc6316a364e6942047352d_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/2973ebdecf9c4e04a6bc6316a364e6942047352d_2880x1620.jpg?"/>
    </item>
    <item>
      <title>How to practice safe sexting | Amy Adele Hasinoff</title>
      <itunes:author>Amy Adele Hasinoff</itunes:author>
      <description>
        <![CDATA[Sexting, like anything that's fun, runs its risks -- but a serious violation of privacy shouldn't be one of them. Amy Adele Hasinoff looks at problematic responses to sexting in mass media, law and education, offering practical solutions for how individuals and tech companies can protect sensitive (and, ahem, potentially scandalous) digital files.]]>
      </description>
      <itunes:subtitle>How to practice safe sexting | Amy Adele Hasinoff</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Sexting, like anything that's fun, runs its risks -- but a serious violation of privacy shouldn't be one of them. Amy Adele Hasinoff looks at problematic responses to sexting in mass media, law and education, offering practical solutions for how individuals and tech companies can protect sensitive (and, ahem, potentially scandalous) digital files.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/AmyAdeleHasinoff_2016X.mp4?apikey=TEDRSS" length="49767695"/>
      <link>https://www.ted.com/talks/amy_adele_hasinoff_how_to_practice_safe_sexting?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2685</guid>
      <jwplayer:talkId>2685</jwplayer:talkId>
      <pubDate>Tue, 14 Feb 2017 16:03:08 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:14:25</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/AmyAdeleHasinoff_2016X.mp4?apikey=TEDRSS" fileSize="49767695" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/05717986c9c5cd865d11bd48849f5a5bea90a922_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/05717986c9c5cd865d11bd48849f5a5bea90a922_2880x1620.jpg?"/>
    </item>
    <item>
      <title>An electrifying acoustic guitar performance |  Rodrigo y Gabriela</title>
      <itunes:author> Rodrigo y Gabriela</itunes:author>
      <description>
        <![CDATA[Guitar duo Rodrigo y Gabriela combine furiously fast riffs and dazzling rhythms to create a style that draws on both flamenco guitar and heavy metal in this live performance of their song, "The Soundmaker."]]>
      </description>
      <itunes:subtitle>An electrifying acoustic guitar performance |  Rodrigo y Gabriela</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Guitar duo Rodrigo y Gabriela combine furiously fast riffs and dazzling rhythms to create a style that draws on both flamenco guitar and heavy metal in this live performance of their song, "The Soundmaker."]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/RodrigoYGabriela_2015.mp4?apikey=TEDRSS" length="28984204"/>
      <link>https://www.ted.com/talks/rodrigo_y_gabriela_an_electrifying_acoustic_guitar_performance?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2684</guid>
      <jwplayer:talkId>2684</jwplayer:talkId>
      <pubDate>Tue, 14 Feb 2017 11:57:08 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:04:17</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/RodrigoYGabriela_2015.mp4?apikey=TEDRSS" fileSize="28984204" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/5f9c005e712ba7dc695a395350bd7679ef6ae2d4_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/5f9c005e712ba7dc695a395350bd7679ef6ae2d4_2880x1620.jpg?"/>
    </item>
    <item>
      <title>How racism harms pregnant women -- and what can help | Miriam Zoila Pérez</title>
      <itunes:author>Miriam Zoila Pérez</itunes:author>
      <description>
        <![CDATA[Racism is making people sick -- especially black women and babies, says Miriam Zoila Pérez. The doula turned journalist explores the relationship between race, class and illness and tells us about a radically compassionate prenatal care program that can buffer pregnant women from the stress that people of color face every day.]]>
      </description>
      <itunes:subtitle>How racism harms pregnant women -- and what can help | Miriam Zoila Pérez</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Racism is making people sick -- especially black women and babies, says Miriam Zoila Pérez. The doula turned journalist explores the relationship between race, class and illness and tells us about a radically compassionate prenatal care program that can buffer pregnant women from the stress that people of color face every day.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/MiriamZoilaPerez_2016W.mp4?apikey=TEDRSS" length="56437298"/>
      <link>https://www.ted.com/talks/miriam_zoila_perez_how_racism_harms_pregnant_women_and_what_can_help?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2686</guid>
      <jwplayer:talkId>2686</jwplayer:talkId>
      <pubDate>Mon, 13 Feb 2017 16:05:56 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:12:25</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/MiriamZoilaPerez_2016W.mp4?apikey=TEDRSS" fileSize="56437298" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/5c10b95602af3e030f0cbc8d8d17149c7c2e88b1_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/5c10b95602af3e030f0cbc8d8d17149c7c2e88b1_2880x1620.jpg?"/>
    </item>
    <item>
      <title>What it's like to be a parent in a war zone | Aala El-Khani</title>
      <itunes:author>Aala El-Khani</itunes:author>
      <description>
        <![CDATA[How do parents protect their children and help them feel secure again when their homes are ripped apart by war? In this warm-hearted talk, psychologist Aala El-Khani shares her work supporting -- and learning from -- refugee families affected by the civil war in Syria. She asks: How can we help these loving parents give their kids the warm, secure parenting they most need?]]>
      </description>
      <itunes:subtitle>What it's like to be a parent in a war zone | Aala El-Khani</itunes:subtitle>
      <itunes:summary>
        <![CDATA[How do parents protect their children and help them feel secure again when their homes are ripped apart by war? In this warm-hearted talk, psychologist Aala El-Khani shares her work supporting -- and learning from -- refugee families affected by the civil war in Syria. She asks: How can we help these loving parents give their kids the warm, secure parenting they most need?]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/AalaElKhani_2016X.mp4?apikey=TEDRSS" length="48867695"/>
      <link>https://www.ted.com/talks/aala_el_khani_what_it_s_like_to_be_a_parent_in_a_war_zone?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2683</guid>
      <jwplayer:talkId>2683</jwplayer:talkId>
      <pubDate>Fri, 10 Feb 2017 15:49:37 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:14:16</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/AalaElKhani_2016X.mp4?apikey=TEDRSS" fileSize="48867695" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/6f7b45bd8e347e6e4a2aac9f1806a85d68793e5c_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/6f7b45bd8e347e6e4a2aac9f1806a85d68793e5c_2880x1620.jpg?"/>
    </item>
    <item>
      <title>4 ways to make a city more walkable | Jeff Speck</title>
      <itunes:author>Jeff Speck</itunes:author>
      <description>
        <![CDATA[Freedom from cars, freedom from sprawl, freedom to walk your city! City planner Jeff Speck shares his "general theory of walkability" -- four planning principles to transform sprawling cities of six-lane highways and 600-foot blocks into safe, walkable oases full of bike lanes and tree-lined streets.]]>
      </description>
      <itunes:subtitle>4 ways to make a city more walkable | Jeff Speck</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Freedom from cars, freedom from sprawl, freedom to walk your city! City planner Jeff Speck shares his "general theory of walkability" -- four planning principles to transform sprawling cities of six-lane highways and 600-foot blocks into safe, walkable oases full of bike lanes and tree-lined streets.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/JeffSpeck_2013X.mp4?apikey=TEDRSS" length="76023833"/>
      <link>https://www.ted.com/talks/jeff_speck_4_ways_to_make_a_city_more_walkable?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2682</guid>
      <jwplayer:talkId>2682</jwplayer:talkId>
      <pubDate>Thu, 09 Feb 2017 15:55:14 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:18:37</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/JeffSpeck_2013X.mp4?apikey=TEDRSS" fileSize="76023833" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/5344a548b578587ac392c3e05e0e604f55371d94_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/5344a548b578587ac392c3e05e0e604f55371d94_2880x1620.jpg?"/>
    </item>
    <item>
      <title>New nanotech to detect cancer early | Joshua Smith</title>
      <itunes:author>Joshua Smith</itunes:author>
      <description>
        <![CDATA[What if every home had an early-warning cancer detection system? Researcher Joshua Smith is developing a nanobiotechnology "cancer alarm" that scans for traces of disease in the form of special biomarkers called exosomes. In this forward-thinking talk, he shares his dream for how we might revolutionize cancer detection and, ultimately, save lives.]]>
      </description>
      <itunes:subtitle>New nanotech to detect cancer early | Joshua Smith</itunes:subtitle>
      <itunes:summary>
        <![CDATA[What if every home had an early-warning cancer detection system? Researcher Joshua Smith is developing a nanobiotechnology "cancer alarm" that scans for traces of disease in the form of special biomarkers called exosomes. In this forward-thinking talk, he shares his dream for how we might revolutionize cancer detection and, ultimately, save lives.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/JoshuaSmith_2016S.mp4?apikey=TEDRSS" length="49716458"/>
      <link>https://www.ted.com/talks/joshua_smith_new_nanotech_to_catch_cancer_early?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2680</guid>
      <jwplayer:talkId>2680</jwplayer:talkId>
      <pubDate>Wed, 08 Feb 2017 15:58:51 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:12:26</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/JoshuaSmith_2016S.mp4?apikey=TEDRSS" fileSize="49716458" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/43936e67c46e89faf0c1f486095bcafa38c1aab6_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/43936e67c46e89faf0c1f486095bcafa38c1aab6_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Our story of rape and reconciliation | Thordis Elva and Tom Stranger</title>
      <itunes:author>Thordis Elva and Tom Stranger</itunes:author>
      <description>
        <![CDATA[In 1996, Thordis Elva shared a teenage romance with Tom Stranger, an exchange student from Australia. After a school dance, Tom raped Thordis, after which they parted ways for many years. In this extraordinary talk, Elva and Stranger move through a years-long chronology of shame and silence, and invite us to discuss the omnipresent global issue of sexual violence in a new, honest way. For a Q&A with the speakers, visit go.ted.com/thordisandtom.]]>
      </description>
      <itunes:subtitle>Our story of rape and reconciliation | Thordis Elva and Tom Stranger</itunes:subtitle>
      <itunes:summary>
        <![CDATA[In 1996, Thordis Elva shared a teenage romance with Tom Stranger, an exchange student from Australia. After a school dance, Tom raped Thordis, after which they parted ways for many years. In this extraordinary talk, Elva and Stranger move through a years-long chronology of shame and silence, and invite us to discuss the omnipresent global issue of sexual violence in a new, honest way. For a Q&A with the speakers, visit go.ted.com/thordisandtom.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/ThordisElvaandTomStranger_2016W.mp4?apikey=TEDRSS" length="65356574"/>
      <link>https://www.ted.com/talks/thordis_elva_tom_stranger_our_story_of_rape_and_reconciliation?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2678</guid>
      <jwplayer:talkId>2678</jwplayer:talkId>
      <pubDate>Tue, 07 Feb 2017 11:44:34 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:19:06</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/ThordisElvaandTomStranger_2016W.mp4?apikey=TEDRSS" fileSize="65356574" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/698bb104dda80250739b46f02190f74526ba2ed4_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/698bb104dda80250739b46f02190f74526ba2ed4_2880x1620.jpg?"/>
    </item>
    <item>
      <title>The incredible inventions of intuitive AI | Maurice Conti</title>
      <itunes:author>Maurice Conti</itunes:author>
      <description>
        <![CDATA[What do you get when you give a design tool a digital nervous system? Computers that improve our ability to think and imagine, and robotic systems that come up with (and build) radical new designs for bridges, cars, drones and much more -- all by themselves. Take a tour of the Augmented Age with futurist Maurice Conti and preview a time when robots and humans will work side-by-side to accomplish things neither could do alone.]]>
      </description>
      <itunes:subtitle>The incredible inventions of intuitive AI | Maurice Conti</itunes:subtitle>
      <itunes:summary>
        <![CDATA[What do you get when you give a design tool a digital nervous system? Computers that improve our ability to think and imagine, and robotic systems that come up with (and build) radical new designs for bridges, cars, drones and much more -- all by themselves. Take a tour of the Augmented Age with futurist Maurice Conti and preview a time when robots and humans will work side-by-side to accomplish things neither could do alone.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/MauriceConti_2016X.mp4?apikey=TEDRSS" length="62454202"/>
      <link>https://www.ted.com/talks/maurice_conti_the_incredible_inventions_of_intuitive_ai?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2671</guid>
      <jwplayer:talkId>2671</jwplayer:talkId>
      <pubDate>Mon, 06 Feb 2017 15:48:32 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:15:23</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/MauriceConti_2016X.mp4?apikey=TEDRSS" fileSize="62454202" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/230aaccbc41c53f9225a0e86fb4ae3ad7578f595_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/230aaccbc41c53f9225a0e86fb4ae3ad7578f595_2880x1620.jpg?"/>
    </item>
    <item>
      <title>What time is it on Mars? | Nagin Cox</title>
      <itunes:author>Nagin Cox</itunes:author>
      <description>
        <![CDATA[Nagin Cox is a first-generation Martian. As a spacecraft engineer at NASA's Jet Propulsion Laboratory, Cox works on the team that manages the United States' rovers on Mars. But working a 9-to-5 on another planet -- whose day is 40 minutes longer than Earth's -- has particular, often comical challenges.]]>
      </description>
      <itunes:subtitle>What time is it on Mars? | Nagin Cox</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Nagin Cox is a first-generation Martian. As a spacecraft engineer at NASA's Jet Propulsion Laboratory, Cox works on the team that manages the United States' rovers on Mars. But working a 9-to-5 on another planet -- whose day is 40 minutes longer than Earth's -- has particular, often comical challenges.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/NaginCox_2016X.mp4?apikey=TEDRSS" length="49285150"/>
      <link>https://www.ted.com/talks/nagin_cox_what_time_is_it_on_mars?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2677</guid>
      <jwplayer:talkId>2677</jwplayer:talkId>
      <pubDate>Fri, 03 Feb 2017 16:06:45 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:13:47</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/NaginCox_2016X.mp4?apikey=TEDRSS" fileSize="49285150" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/62d8533e4c1069b858f2fe52a24bef69dcf07aa7_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/62d8533e4c1069b858f2fe52a24bef69dcf07aa7_2880x1620.jpg?"/>
    </item>
    <item>
      <title>My son was a Columbine shooter. This is my story | Sue Klebold</title>
      <itunes:author>Sue Klebold</itunes:author>
      <description>
        <![CDATA[Sue Klebold is the mother of Dylan Klebold, one of the two shooters who committed the Columbine High School massacre, murdering 12 students and a teacher. She's spent years excavating every detail of her family life, trying to understand what she could have done to prevent her son's violence. In this difficult, jarring talk, Klebold explores the intersection between mental health and violence, advocating for parents and professionals to continue to examine the link between suicidal and homicidal thinking.]]>
      </description>
      <itunes:subtitle>My son was a Columbine shooter. This is my story | Sue Klebold</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Sue Klebold is the mother of Dylan Klebold, one of the two shooters who committed the Columbine High School massacre, murdering 12 students and a teacher. She's spent years excavating every detail of her family life, trying to understand what she could have done to prevent her son's violence. In this difficult, jarring talk, Klebold explores the intersection between mental health and violence, advocating for parents and professionals to continue to examine the link between suicidal and homicidal thinking.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/SueKlebold_2016P.mp4?apikey=TEDRSS" length="60005966"/>
      <link>https://www.ted.com/talks/sue_klebold_my_son_was_a_columbine_shooter_this_is_my_story?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2681</guid>
      <jwplayer:talkId>2681</jwplayer:talkId>
      <pubDate>Thu, 02 Feb 2017 16:02:52 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:15:18</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/SueKlebold_2016P.mp4?apikey=TEDRSS" fileSize="60005966" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/b7eeb69c4b0db1794ff93a45d3197bde6ac21c7f_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/b7eeb69c4b0db1794ff93a45d3197bde6ac21c7f_2880x1620.jpg?"/>
    </item>
    <item>
      <title>How to get better at the things you care about | Eduardo Briceño</title>
      <itunes:author>Eduardo Briceño</itunes:author>
      <description>
        <![CDATA[Working hard but not improving? You're not alone. Eduardo Briceño reveals a simple way to think about getting better at the things you do, whether that's work, parenting or creative hobbies. And he shares some useful techniques so you can keep learning and always feel like you're moving forward.]]>
      </description>
      <itunes:subtitle>How to get better at the things you care about | Eduardo Briceño</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Working hard but not improving? You're not alone. Eduardo Briceño reveals a simple way to think about getting better at the things you do, whether that's work, parenting or creative hobbies. And he shares some useful techniques so you can keep learning and always feel like you're moving forward.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/EduardoBriceno_2016X.mp4?apikey=TEDRSS" length="39194501"/>
      <link>https://www.ted.com/talks/eduardo_briceno_how_to_get_better_at_the_things_you_care_about?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2679</guid>
      <jwplayer:talkId>2679</jwplayer:talkId>
      <pubDate>Wed, 01 Feb 2017 16:05:43 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:11:22</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/EduardoBriceno_2016X.mp4?apikey=TEDRSS" fileSize="39194501" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/0a8d8b5fbcafc4391d93c8dcd27d475ab07b9ca6_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/0a8d8b5fbcafc4391d93c8dcd27d475ab07b9ca6_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Why you should love statistics | Alan Smith</title>
      <itunes:author>Alan Smith</itunes:author>
      <description>
        <![CDATA[Think you're good at guessing stats? Guess again. Whether we consider ourselves math people or not, our ability to understand and work with numbers is terribly limited, says data visualization expert Alan Smith. In this delightful talk, Smith explores the mismatch between what we know and what we think we know.]]>
      </description>
      <itunes:subtitle>Why you should love statistics | Alan Smith</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Think you're good at guessing stats? Guess again. Whether we consider ourselves math people or not, our ability to understand and work with numbers is terribly limited, says data visualization expert Alan Smith. In this delightful talk, Smith explores the mismatch between what we know and what we think we know.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/AlanSmith_2016X.mp4?apikey=TEDRSS" length="44182956"/>
      <link>https://www.ted.com/talks/alan_smith_why_we_re_so_bad_at_statistics?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2674</guid>
      <jwplayer:talkId>2674</jwplayer:talkId>
      <pubDate>Tue, 31 Jan 2017 15:39:32 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:12:49</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/AlanSmith_2016X.mp4?apikey=TEDRSS" fileSize="44182956" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/494f688e9688a52d75352d2c45dbf3e0729a5c60_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/494f688e9688a52d75352d2c45dbf3e0729a5c60_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Help discover ancient ruins -- before it's too late | Sarah Parcak</title>
      <itunes:author>Sarah Parcak</itunes:author>
      <description>
        <![CDATA[Sarah Parcak uses satellites orbiting hundreds of miles above Earth to uncover hidden ancient treasures buried beneath our feet. There's a lot to discover; in the Egyptian Delta alone, Parcak estimates we've excavated less than a thousandth of one percent of what's out there. Now, with the 2016 TED Prize and an infectious enthusiasm for archaeology, she's developed an online platform called GlobalXplorer that enables anyone with an internet connection to discover unknown sites and protect what remains of our shared human inheritance.]]>
      </description>
      <itunes:subtitle>Help discover ancient ruins -- before it's too late | Sarah Parcak</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Sarah Parcak uses satellites orbiting hundreds of miles above Earth to uncover hidden ancient treasures buried beneath our feet. There's a lot to discover; in the Egyptian Delta alone, Parcak estimates we've excavated less than a thousandth of one percent of what's out there. Now, with the 2016 TED Prize and an infectious enthusiasm for archaeology, she's developed an online platform called GlobalXplorer that enables anyone with an internet connection to discover unknown sites and protect what remains of our shared human inheritance.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/SarahParcak_2016.mp4?apikey=TEDRSS" length="85007939"/>
      <link>https://www.ted.com/talks/sarah_parcak_help_discover_ancient_ruins_before_it_s_too_late?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2673</guid>
      <jwplayer:talkId>2673</jwplayer:talkId>
      <pubDate>Mon, 30 Jan 2017 15:48:37 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:21:48</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/SarahParcak_2016.mp4?apikey=TEDRSS" fileSize="85007939" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/112f4f9d7cdb317e82e55e3a67f4bd098ef15512_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/112f4f9d7cdb317e82e55e3a67f4bd098ef15512_2880x1620.jpg?"/>
    </item>
    <item>
      <title>A young scientist's quest for clean water | Deepika Kurup</title>
      <itunes:author>Deepika Kurup</itunes:author>
      <description>
        <![CDATA[Deepika Kurup has been determined to solve the global water crisis since she was 14 years old, after she saw kids outside her grandparents' house in India drinking water that looked too dirty even to touch. Her research began in her family kitchen -- and eventually led to a major science prize. Hear how this teenage scientist developed a cost-effective, eco-friendly way to purify water. ]]>
      </description>
      <itunes:subtitle>A young scientist's quest for clean water | Deepika Kurup</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Deepika Kurup has been determined to solve the global water crisis since she was 14 years old, after she saw kids outside her grandparents' house in India drinking water that looked too dirty even to touch. Her research began in her family kitchen -- and eventually led to a major science prize. Hear how this teenage scientist developed a cost-effective, eco-friendly way to purify water. ]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/DeepikaKurup_2016W.mp4?apikey=TEDRSS" length="40689627"/>
      <link>https://www.ted.com/talks/deepika_kurup_a_young_scientist_s_quest_for_clean_water?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2672</guid>
      <jwplayer:talkId>2672</jwplayer:talkId>
      <pubDate>Fri, 27 Jan 2017 16:04:51 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:07:59</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/DeepikaKurup_2016W.mp4?apikey=TEDRSS" fileSize="40689627" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/36a01016eb74b112d7b3bd0318372d6f5cc172a9_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/36a01016eb74b112d7b3bd0318372d6f5cc172a9_2880x1620.jpg?"/>
    </item>
    <item>
      <title>What we don't know about Europe's Muslim kids | Deeyah Khan</title>
      <itunes:author>Deeyah Khan</itunes:author>
      <description>
        <![CDATA[As the child of an Afghan mother and Pakistani father raised in Norway, Deeyah Khan knows what it's like to be a young person stuck between your community and your country. In this powerful, emotional talk, the filmmaker unearths the rejection and isolation felt by many Muslim kids growing up in the West -- and the deadly consequences of not embracing our youth before extremist groups do.]]>
      </description>
      <itunes:subtitle>What we don't know about Europe's Muslim kids | Deeyah Khan</itunes:subtitle>
      <itunes:summary>
        <![CDATA[As the child of an Afghan mother and Pakistani father raised in Norway, Deeyah Khan knows what it's like to be a young person stuck between your community and your country. In this powerful, emotional talk, the filmmaker unearths the rejection and isolation felt by many Muslim kids growing up in the West -- and the deadly consequences of not embracing our youth before extremist groups do.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/DeeyahKhan_2016X.mp4?apikey=TEDRSS" length="78579159"/>
      <link>https://www.ted.com/talks/deeyah_khan_what_we_don_t_know_about_europe_s_muslim_kids?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2662</guid>
      <jwplayer:talkId>2662</jwplayer:talkId>
      <pubDate>Thu, 26 Jan 2017 16:02:23 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:20:11</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/DeeyahKhan_2016X.mp4?apikey=TEDRSS" fileSize="78579159" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/f9c6e5c8f59c0c2ac792dcf22920519696bd3ad3_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/f9c6e5c8f59c0c2ac792dcf22920519696bd3ad3_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Where is cybercrime really coming from? | Caleb Barlow</title>
      <itunes:author>Caleb Barlow</itunes:author>
      <description>
        <![CDATA[Cybercrime netted a whopping $450 billion in profits last year, with 2 billion records lost or stolen worldwide. Security expert Caleb Barlow calls out the insufficiency of our current strategies to protect our data. His solution? We need to respond to cybercrime with the same collective effort as we apply to a health care crisis, sharing timely information on who is infected and how the disease is spreading. If we're not sharing, he says, then we're part of the problem.]]>
      </description>
      <itunes:subtitle>Where is cybercrime really coming from? | Caleb Barlow</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Cybercrime netted a whopping $450 billion in profits last year, with 2 billion records lost or stolen worldwide. Security expert Caleb Barlow calls out the insufficiency of our current strategies to protect our data. His solution? We need to respond to cybercrime with the same collective effort as we apply to a health care crisis, sharing timely information on who is infected and how the disease is spreading. If we're not sharing, he says, then we're part of the problem.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/CalebBarlow_2016S.mp4?apikey=TEDRSS" length="65319462"/>
      <link>https://www.ted.com/talks/caleb_barlow_where_is_cybercrime_really_coming_from?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2668</guid>
      <jwplayer:talkId>2668</jwplayer:talkId>
      <pubDate>Wed, 25 Jan 2017 16:12:56 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:14:27</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/CalebBarlow_2016S.mp4?apikey=TEDRSS" fileSize="65319462" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/bb8680376dcbdcaa63f8046ea8342d313d92761b_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/bb8680376dcbdcaa63f8046ea8342d313d92761b_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Buildings that blend nature and city | Jeanne Gang</title>
      <itunes:author>Jeanne Gang</itunes:author>
      <description>
        <![CDATA[A skyscraper that channels the breeze ... a building that creates community around a hearth ... Jeanne Gang uses architecture to build relationships. In this engaging tour of her work, Gang invites us into buildings large and small, from a surprising local community center to a landmark Chicago skyscraper. "Through architecture, we can do much more than create buildings," she says. "We can help steady this planet we all share."]]>
      </description>
      <itunes:subtitle>Buildings that blend nature and city | Jeanne Gang</itunes:subtitle>
      <itunes:summary>
        <![CDATA[A skyscraper that channels the breeze ... a building that creates community around a hearth ... Jeanne Gang uses architecture to build relationships. In this engaging tour of her work, Gang invites us into buildings large and small, from a surprising local community center to a landmark Chicago skyscraper. "Through architecture, we can do much more than create buildings," she says. "We can help steady this planet we all share."]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/JeanneGang_2016W.mp4?apikey=TEDRSS" length="48645916"/>
      <link>https://www.ted.com/talks/jeanne_gang_buildings_that_blend_nature_and_city?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2670</guid>
      <jwplayer:talkId>2670</jwplayer:talkId>
      <pubDate>Tue, 24 Jan 2017 16:13:03 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:11:55</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/JeanneGang_2016W.mp4?apikey=TEDRSS" fileSize="48645916" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/90867c364d093ffc416e7827b63a9302902fcfa4_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/90867c364d093ffc416e7827b63a9302902fcfa4_2880x1620.jpg?"/>
    </item>
    <item>
      <title>The ethical dilemma of designer babies | Paul Knoepfler</title>
      <itunes:author>Paul Knoepfler</itunes:author>
      <description>
        <![CDATA[Creating genetically modified people is no longer a science fiction fantasy; it's a likely future scenario. Biologist Paul Knoepfler estimates that within fifteen years, scientists could use the gene editing technology CRISPR to make certain "upgrades" to human embryos -- from altering physical appearances to eliminating the risk of auto-immune diseases. In this thought-provoking talk, Knoepfler readies us for the coming designer baby revolution and its very personal, and unforeseeable, consequences.]]>
      </description>
      <itunes:subtitle>The ethical dilemma of designer babies | Paul Knoepfler</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Creating genetically modified people is no longer a science fiction fantasy; it's a likely future scenario. Biologist Paul Knoepfler estimates that within fifteen years, scientists could use the gene editing technology CRISPR to make certain "upgrades" to human embryos -- from altering physical appearances to eliminating the risk of auto-immune diseases. In this thought-provoking talk, Knoepfler readies us for the coming designer baby revolution and its very personal, and unforeseeable, consequences.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/PaulKnoepfler_2015X.mp4?apikey=TEDRSS" length="67661857"/>
      <link>https://www.ted.com/talks/paul_knoepfler_the_ethical_dilemma_of_designer_babies?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2651</guid>
      <jwplayer:talkId>2651</jwplayer:talkId>
      <pubDate>Mon, 23 Jan 2017 16:18:29 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:18:19</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/PaulKnoepfler_2015X.mp4?apikey=TEDRSS" fileSize="67661857" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/380f7ff8bdaa85f87e96fe0f4dbff5aa79f00178_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/380f7ff8bdaa85f87e96fe0f4dbff5aa79f00178_2880x1620.jpg?"/>
    </item>
    <item>
      <title>How to have better political conversations | Robb Willer</title>
      <itunes:author>Robb Willer</itunes:author>
      <description>
        <![CDATA[Robb Willer studies the forces that unite and divide us. As a social psychologist, he researches how moral values -- typically a source of division -- can also be used to bring people together. Willer shares compelling insights on how we might bridge the ideological divide and offers some intuitive advice on ways to be more persuasive when talking politics.]]>
      </description>
      <itunes:subtitle>How to have better political conversations | Robb Willer</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Robb Willer studies the forces that unite and divide us. As a social psychologist, he researches how moral values -- typically a source of division -- can also be used to bring people together. Willer shares compelling insights on how we might bridge the ideological divide and offers some intuitive advice on ways to be more persuasive when talking politics.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/RobbWiller_2016X.mp4?apikey=TEDRSS" length="46193714"/>
      <link>https://www.ted.com/talks/robb_willer_how_to_have_better_political_conversations?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2669</guid>
      <jwplayer:talkId>2669</jwplayer:talkId>
      <pubDate>Fri, 20 Jan 2017 16:07:08 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:12:01</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/RobbWiller_2016X.mp4?apikey=TEDRSS" fileSize="46193714" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/f3187c6a62c88e3d8123e39f9d8180fa6d1aea86_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/f3187c6a62c88e3d8123e39f9d8180fa6d1aea86_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Art made of the air we breathe | Emily Parsons-Lord</title>
      <itunes:author>Emily Parsons-Lord</itunes:author>
      <description>
        <![CDATA[Emily Parsons-Lord re-creates air from distinct moments in Earth's history -- from the clean, fresh-tasting air of the Carboniferous period to the soda-water air of the Great Dying to the heavy, toxic air of the future we're creating. By turning air into art, she invites us to know the invisible world around us. Breathe in the Earth's past and future in this imaginative, trippy talk.]]>
      </description>
      <itunes:subtitle>Art made of the air we breathe | Emily Parsons-Lord</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Emily Parsons-Lord re-creates air from distinct moments in Earth's history -- from the clean, fresh-tasting air of the Carboniferous period to the soda-water air of the Great Dying to the heavy, toxic air of the future we're creating. By turning air into art, she invites us to know the invisible world around us. Breathe in the Earth's past and future in this imaginative, trippy talk.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/EmilyParsonsLord_2016X.mp4?apikey=TEDRSS" length="47747835"/>
      <link>https://www.ted.com/talks/emily_parsons_lord_art_made_of_the_air_we_breathe?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2667</guid>
      <jwplayer:talkId>2667</jwplayer:talkId>
      <pubDate>Thu, 19 Jan 2017 15:57:51 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:10:49</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/EmilyParsonsLord_2016X.mp4?apikey=TEDRSS" fileSize="47747835" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/50b7b091753ff1f7e1e3e9847f2103d730547525_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/50b7b091753ff1f7e1e3e9847f2103d730547525_2880x1620.jpg?"/>
    </item>
    <item>
      <title>How online abuse of women has spiraled out of control | Ashley Judd</title>
      <itunes:author>Ashley Judd</itunes:author>
      <description>
        <![CDATA[Enough with online hate speech, sexual harassment and threats of violence against women and marginalized groups. It's time to take the global crisis of online abuse seriously. In this searching, powerful talk, Ashley Judd recounts her ongoing experience of being terrorized on social media for her unwavering activism and calls on citizens of the internet, the tech community, law enforcement and legislators to recognize the offline harm of online harassment.]]>
      </description>
      <itunes:subtitle>How online abuse of women has spiraled out of control | Ashley Judd</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Enough with online hate speech, sexual harassment and threats of violence against women and marginalized groups. It's time to take the global crisis of online abuse seriously. In this searching, powerful talk, Ashley Judd recounts her ongoing experience of being terrorized on social media for her unwavering activism and calls on citizens of the internet, the tech community, law enforcement and legislators to recognize the offline harm of online harassment.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/AshleyJudd_2016W.mp4?apikey=TEDRSS" length="55468251"/>
      <link>https://www.ted.com/talks/ashley_judd_how_online_abuse_of_women_has_spiraled_out_of_control?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2666</guid>
      <jwplayer:talkId>2666</jwplayer:talkId>
      <pubDate>Wed, 18 Jan 2017 15:55:34 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:16:10</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/AshleyJudd_2016W.mp4?apikey=TEDRSS" fileSize="55468251" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/7bc74e43fd1ea88b0a7009affb55d6c753b848c2_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/7bc74e43fd1ea88b0a7009affb55d6c753b848c2_2880x1620.jpg?"/>
    </item>
    <item>
      <title>What happens when you have a disease doctors can't diagnose | Jennifer Brea</title>
      <itunes:author>Jennifer Brea</itunes:author>
      <description>
        <![CDATA[Five years ago, TED Fellow Jennifer Brea became progressively ill with myalgic encephalomyelitis, commonly known as chronic fatigue syndrome, a debilitating illness that severely impairs normal activities and on bad days makes even the rustling of bed sheets unbearable. In this poignant talk, Brea describes the obstacles she's encountered in seeking treatment for her condition, whose root causes and physical effects we don't fully understand, as well as her mission to document through film the lives of patients that medicine struggles to treat.]]>
      </description>
      <itunes:subtitle>What happens when you have a disease doctors can't diagnose | Jennifer Brea</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Five years ago, TED Fellow Jennifer Brea became progressively ill with myalgic encephalomyelitis, commonly known as chronic fatigue syndrome, a debilitating illness that severely impairs normal activities and on bad days makes even the rustling of bed sheets unbearable. In this poignant talk, Brea describes the obstacles she's encountered in seeking treatment for her condition, whose root causes and physical effects we don't fully understand, as well as her mission to document through film the lives of patients that medicine struggles to treat.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/JenniferBrea_2016T.mp4?apikey=TEDRSS" length="70733036"/>
      <link>https://www.ted.com/talks/jen_brea_what_happens_when_you_have_a_disease_doctors_can_t_diagnose?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2627</guid>
      <jwplayer:talkId>2627</jwplayer:talkId>
      <pubDate>Tue, 17 Jan 2017 16:09:16 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:17:07</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/JenniferBrea_2016T.mp4?apikey=TEDRSS" fileSize="70733036" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/806fae883bd8d18133ee02ba5f67152396d9864a_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/806fae883bd8d18133ee02ba5f67152396d9864a_2880x1620.jpg?"/>
    </item>
    <item>
      <title>If a story moves you, act on it | Sisonke Msimang</title>
      <itunes:author>Sisonke Msimang</itunes:author>
      <description>
        <![CDATA[Stories are necessary, but they're not as magical as they seem, says writer Sisonke Msimang. In this funny and thoughtful talk, Msimang questions our emphasis on storytelling and spotlights the decline of facts. During a critical time when listening has been confused for action, Msimang asks us to switch off our phones, step away from our screens and step out into the real world to create a plan for justice.]]>
      </description>
      <itunes:subtitle>If a story moves you, act on it | Sisonke Msimang</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Stories are necessary, but they're not as magical as they seem, says writer Sisonke Msimang. In this funny and thoughtful talk, Msimang questions our emphasis on storytelling and spotlights the decline of facts. During a critical time when listening has been confused for action, Msimang asks us to switch off our phones, step away from our screens and step out into the real world to create a plan for justice.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/SisonkeMsimang_2016W.mp4?apikey=TEDRSS" length="58599155"/>
      <link>https://www.ted.com/talks/sisonke_msimang_if_a_story_moves_you_act_on_it?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2665</guid>
      <jwplayer:talkId>2665</jwplayer:talkId>
      <pubDate>Fri, 13 Jan 2017 16:19:34 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:12:46</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/SisonkeMsimang_2016W.mp4?apikey=TEDRSS" fileSize="58599155" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/564e26692522c2582395c0b4259e51275c2d8a8d_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/564e26692522c2582395c0b4259e51275c2d8a8d_2880x1620.jpg?"/>
    </item>
    <item>
      <title>To solve old problems, study new species | Alejandro Sánchez Alvarado</title>
      <itunes:author>Alejandro Sánchez Alvarado</itunes:author>
      <description>
        <![CDATA[Nature is wonderfully abundant, diverse and mysterious -- but biological research today tends to focus on only seven species, including rats, chickens, fruit flies and us. We're studying an astonishingly narrow sliver of life, says biologist Alejandro Sánchez Alvarado, and hoping it'll be enough to solve the oldest, most challenging problems in science, like cancer. In this visually captivating talk, Alvarado calls on us to interrogate the unknown and shows us the remarkable discoveries that surface when we do.]]>
      </description>
      <itunes:subtitle>To solve old problems, study new species | Alejandro Sánchez Alvarado</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Nature is wonderfully abundant, diverse and mysterious -- but biological research today tends to focus on only seven species, including rats, chickens, fruit flies and us. We're studying an astonishingly narrow sliver of life, says biologist Alejandro Sánchez Alvarado, and hoping it'll be enough to solve the oldest, most challenging problems in science, like cancer. In this visually captivating talk, Alvarado calls on us to interrogate the unknown and shows us the remarkable discoveries that surface when we do.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/AlejandroAlvarado_2016X.mp4?apikey=TEDRSS" length="43606541"/>
      <link>https://www.ted.com/talks/alejandro_sanchez_alvarado_to_solve_old_problems_study_new_species?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2663</guid>
      <jwplayer:talkId>2663</jwplayer:talkId>
      <pubDate>Thu, 12 Jan 2017 16:07:48 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:12:39</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/AlejandroAlvarado_2016X.mp4?apikey=TEDRSS" fileSize="43606541" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/fb2f9348098ef1726ee090f63612c0f729ffa2a4_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/fb2f9348098ef1726ee090f63612c0f729ffa2a4_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Meet the inventor of the electronic spreadsheet | Dan Bricklin</title>
      <itunes:author>Dan Bricklin</itunes:author>
      <description>
        <![CDATA[Dan Bricklin changed the world forever when he codeveloped VisiCalc, the first electronic spreadsheet and grandfather of programs you probably use every day like Microsoft Excel and Google Sheets. Join the software engineer and computing legend as he explores the tangled web of first jobs, daydreams and homework problems that led to his transformational invention.]]>
      </description>
      <itunes:subtitle>Meet the inventor of the electronic spreadsheet | Dan Bricklin</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Dan Bricklin changed the world forever when he codeveloped VisiCalc, the first electronic spreadsheet and grandfather of programs you probably use every day like Microsoft Excel and Google Sheets. Join the software engineer and computing legend as he explores the tangled web of first jobs, daydreams and homework problems that led to his transformational invention.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/DanBricklin_2016X.mp4?apikey=TEDRSS" length="47955021"/>
      <link>https://www.ted.com/talks/dan_bricklin_meet_the_inventor_of_the_electronic_spreadsheet?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2664</guid>
      <jwplayer:talkId>2664</jwplayer:talkId>
      <pubDate>Wed, 11 Jan 2017 15:52:50 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:12:00</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/DanBricklin_2016X.mp4?apikey=TEDRSS" fileSize="47955021" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/092f184f6625c2aeef10949c8d7b2aa14ba4132b_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/092f184f6625c2aeef10949c8d7b2aa14ba4132b_2880x1620.jpg?"/>
    </item>
    <item>
      <title>The next step in nanotechnology | George Tulevski</title>
      <itunes:author>George Tulevski</itunes:author>
      <description>
        <![CDATA[Nearly every other year the transistors that power silicon computer chip shrink in size by half and double in performance, enabling our devices to become more mobile and accessible. But what happens when these components can't get any smaller? George Tulevski researches the unseen and untapped world of nanomaterials. His current work: developing chemical processes to compel billions of carbon nanotubes to assemble themselves into the patterns needed to build circuits, much the same way natural organisms build intricate, diverse and elegant structures. Could they hold the secret to the next generation of computing?]]>
      </description>
      <itunes:subtitle>The next step in nanotechnology | George Tulevski</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Nearly every other year the transistors that power silicon computer chip shrink in size by half and double in performance, enabling our devices to become more mobile and accessible. But what happens when these components can't get any smaller? George Tulevski researches the unseen and untapped world of nanomaterials. His current work: developing chemical processes to compel billions of carbon nanotubes to assemble themselves into the patterns needed to build circuits, much the same way natural organisms build intricate, diverse and elegant structures. Could they hold the secret to the next generation of computing?]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/GeorgeTulevski_2016S.mp4?apikey=TEDRSS" length="44103269"/>
      <link>https://www.ted.com/talks/george_tulevski_the_next_step_in_nanotechnology?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2660</guid>
      <jwplayer:talkId>2660</jwplayer:talkId>
      <pubDate>Tue, 10 Jan 2017 16:19:37 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:09:35</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/GeorgeTulevski_2016S.mp4?apikey=TEDRSS" fileSize="44103269" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/fad632f2c2cca823c60446477a7e5101f3421742_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/fad632f2c2cca823c60446477a7e5101f3421742_2880x1620.jpg?"/>
    </item>
    <item>
      <title>A better way to talk about love | Mandy Len Catron</title>
      <itunes:author>Mandy Len Catron</itunes:author>
      <description>
        <![CDATA[In love, we fall. We're struck, we're crushed, we swoon. We burn with passion. Love makes us crazy and makes us sick. Our hearts ache, and then they break. Talking about love in this way fundamentally shapes how we experience it, says writer Mandy Len Catron. In this talk for anyone who's ever felt crazy in love, Catron highlights a different metaphor for love that may help us find more joy -- and less suffering -- in it.]]>
      </description>
      <itunes:subtitle>A better way to talk about love | Mandy Len Catron</itunes:subtitle>
      <itunes:summary>
        <![CDATA[In love, we fall. We're struck, we're crushed, we swoon. We burn with passion. Love makes us crazy and makes us sick. Our hearts ache, and then they break. Talking about love in this way fundamentally shapes how we experience it, says writer Mandy Len Catron. In this talk for anyone who's ever felt crazy in love, Catron highlights a different metaphor for love that may help us find more joy -- and less suffering -- in it.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/MandyLenCatron_SFU_2015X.mp4?apikey=TEDRSS" length="67073091"/>
      <link>https://www.ted.com/talks/mandy_len_catron_a_better_way_to_talk_about_love?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2661</guid>
      <jwplayer:talkId>2661</jwplayer:talkId>
      <pubDate>Mon, 09 Jan 2017 16:06:20 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:15:17</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/MandyLenCatron_SFU_2015X.mp4?apikey=TEDRSS" fileSize="67073091" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/5d9410d3b8233def4630b48a46e6486333c5a326_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/5d9410d3b8233def4630b48a46e6486333c5a326_2880x1620.jpg?"/>
    </item>
    <item>
      <title>The lies we tell pregnant women | Sofia Jawed-Wessel</title>
      <itunes:author>Sofia Jawed-Wessel</itunes:author>
      <description>
        <![CDATA["When we tell women that sex isn't worth the risk during pregnancy, what we're telling her is that her sexual pleasure doesn't matter ... that she in fact doesn't matter," says sex researcher Sofia Jawed-Wessel. In this eye-opening talk, Jawed-Wessel mines our views about pregnancy and pleasure to lay bare the relationship between women, sex and systems of power.]]>
      </description>
      <itunes:subtitle>The lies we tell pregnant women | Sofia Jawed-Wessel</itunes:subtitle>
      <itunes:summary>
        <![CDATA["When we tell women that sex isn't worth the risk during pregnancy, what we're telling her is that her sexual pleasure doesn't matter ... that she in fact doesn't matter," says sex researcher Sofia Jawed-Wessel. In this eye-opening talk, Jawed-Wessel mines our views about pregnancy and pleasure to lay bare the relationship between women, sex and systems of power.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/SofiaJawedWessel_2016X.mp4?apikey=TEDRSS" length="58553737"/>
      <link>https://www.ted.com/talks/sofia_jawed_wessel_the_lies_we_tell_pregnant_women?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2658</guid>
      <jwplayer:talkId>2658</jwplayer:talkId>
      <pubDate>Fri, 06 Jan 2017 16:15:38 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:14:56</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/SofiaJawedWessel_2016X.mp4?apikey=TEDRSS" fileSize="58553737" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/3dc281fced0320390e74db3bd4d1e392e7d76b57_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/3dc281fced0320390e74db3bd4d1e392e7d76b57_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Want kids to learn well? Feed them well | Sam Kass</title>
      <itunes:author>Sam Kass</itunes:author>
      <description>
        <![CDATA[What can we expect our kids to learn if they're hungry or eating diets full of sugar and empty of nutrients? Former White House Chef and food policymaker Sam Kass discusses the role schools can play in nourishing students' bodies in addition to their minds.]]>
      </description>
      <itunes:subtitle>Want kids to learn well? Feed them well | Sam Kass</itunes:subtitle>
      <itunes:summary>
        <![CDATA[What can we expect our kids to learn if they're hungry or eating diets full of sugar and empty of nutrients? Former White House Chef and food policymaker Sam Kass discusses the role schools can play in nourishing students' bodies in addition to their minds.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/SamKass_2015P.mp4?apikey=TEDRSS" length="53161032"/>
      <link>https://www.ted.com/talks/sam_kass_want_to_teach_kids_well_feed_them_well?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2659</guid>
      <jwplayer:talkId>2659</jwplayer:talkId>
      <pubDate>Thu, 05 Jan 2017 16:11:01 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:12:08</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/SamKass_2015P.mp4?apikey=TEDRSS" fileSize="53161032" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/500c56050b598bbd5837bc0d378a5ba6301e71c0_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/500c56050b598bbd5837bc0d378a5ba6301e71c0_2880x1620.jpg?"/>
    </item>
    <item>
      <title>The world doesn't need more nuclear weapons | Erika Gregory</title>
      <itunes:author>Erika Gregory</itunes:author>
      <description>
        <![CDATA[Today nine nations collectively control more than 15,000 nuclear weapons, each hundreds of times more powerful than those dropped on Hiroshima and Nagasaki. We don't need more nuclear weapons; we need a new generation to face the unfinished challenge of disarmament started decades ago. Nuclear reformer Erika Gregory calls on today's rising leaders -- those born in a time without Cold War fears and duck-and-cover training -- to pursue an ambitious goal: ridding the world of nuclear weapons by 2045.]]>
      </description>
      <itunes:subtitle>The world doesn't need more nuclear weapons | Erika Gregory</itunes:subtitle>
      <itunes:summary>
        <![CDATA[Today nine nations collectively control more than 15,000 nuclear weapons, each hundreds of times more powerful than those dropped on Hiroshima and Nagasaki. We don't need more nuclear weapons; we need a new generation to face the unfinished challenge of disarmament started decades ago. Nuclear reformer Erika Gregory calls on today's rising leaders -- those born in a time without Cold War fears and duck-and-cover training -- to pursue an ambitious goal: ridding the world of nuclear weapons by 2045.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/ErikaGregory_2016W.mp4?apikey=TEDRSS" length="63974901"/>
      <link>https://www.ted.com/talks/erika_gregory_the_world_doesn_t_need_more_nuclear_weapons?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2650</guid>
      <jwplayer:talkId>2650</jwplayer:talkId>
      <pubDate>Wed, 04 Jan 2017 16:10:26 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:14:59</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/ErikaGregory_2016W.mp4?apikey=TEDRSS" fileSize="63974901" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/8d9b1b48279a10e1ce62389fbe37fbc7c2cb66be_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/8d9b1b48279a10e1ce62389fbe37fbc7c2cb66be_2880x1620.jpg?"/>
    </item>
    <item>
      <title>Are you a giver or a taker? | Adam Grant</title>
      <itunes:author>Adam Grant</itunes:author>
      <description>
        <![CDATA[In every workplace, there are three basic kinds of people: givers, takers and matchers. Organizational psychologist Adam Grant breaks down these personalities and offers simple strategies to promote a culture of generosity and keep self-serving employees from taking more than their share.]]>
      </description>
      <itunes:subtitle>Are you a giver or a taker? | Adam Grant</itunes:subtitle>
      <itunes:summary>
        <![CDATA[In every workplace, there are three basic kinds of people: givers, takers and matchers. Organizational psychologist Adam Grant breaks down these personalities and offers simple strategies to promote a culture of generosity and keep self-serving employees from taking more than their share.]]>
      </itunes:summary>
      <enclosure type="video/mp4" url="https://download.ted.com/talks/AdamGrant_2016S.mp4?apikey=TEDRSS" length="46243040"/>
      <link>https://www.ted.com/talks/adam_grant_are_you_a_giver_or_a_taker?rss</link>
      <guid isPermaLink="false">eng.video.talk.ted.com:2652</guid>
      <jwplayer:talkId>2652</jwplayer:talkId>
      <pubDate>Tue, 03 Jan 2017 15:37:47 +0000</pubDate>
      <category>Higher Education</category>
      <itunes:explicit>no</itunes:explicit>
      <itunes:duration>00:13:28</itunes:duration>
      <itunes:keywords>TED</itunes:keywords>
      <media:content url="https://download.ted.com/talks/AdamGrant_2016S.mp4?apikey=TEDRSS" fileSize="46243040" type="video/mp4"/>
      <media:thumbnail url="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/a6526a75389078e7911a2d4dbfec1008d96fe8c2_2880x1620.jpg?h=360&amp;origin=rss&amp;w=480" width="480" height="360"/>
      <itunes:image href="https://pi.tedcdn.com/r/pe.tedcdn.com/images/ted/a6526a75389078e7911a2d4dbfec1008d96fe8c2_2880x1620.jpg?"/>
    </item>
  </channel>
</rss>